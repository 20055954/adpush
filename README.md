###Info###
**AdPush** is an ads serving platform implementing a recommender algorithm. Completed as a final year project for the BSc in Software Systems Development course offered by Waterford Institute of Technology.
##
[Website](http://adpush.herokuapp.com)