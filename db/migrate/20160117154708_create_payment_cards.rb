class CreatePaymentCards < ActiveRecord::Migration
  def change
    create_table :payment_cards do |t|
      t.string :card_num, :unique => true
      t.integer :expiry_date_mm
      t.integer :expiry_date_yy
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
