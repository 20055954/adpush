class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :bank_acc_id
      t.float :amount
      t.date :payment_date

      t.timestamps null: false
    end
  end
end
