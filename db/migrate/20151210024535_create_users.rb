class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login
      t.string :password_digest
      t.string :remember_digest
      t.string :email
      t.string :email_verification_key
      t.integer :utype
      t.string :name
      t.string :company
      t.string :gender
      t.date :dob
      t.string :country
      t.string :state
      t.string :city
      t.string :address
      t.string :zip
      t.boolean :banned, :default => false
      t.float :balance, :default => 0
      t.integer :font_size, :default => 0
      t.integer :color_scheme, :default => 0

      t.timestamps null: false
    end
  end
end
