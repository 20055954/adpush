class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
	  t.integer :user_id
      t.integer :category
      t.text :text
	  t.boolean :addressed, :default => false
	  
      t.timestamps null: false
    end
  end
end
