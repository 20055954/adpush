class CreateBankInfos < ActiveRecord::Migration
  def change
    create_table :bank_infos do |t|
      t.integer :user_id
      t.string :name
      t.string :encrypted_code
      t.string :encrypted_code_salt
      t.string :encrypted_code_iv
      t.string :encrypted_account_number
      t.string :encrypted_account_number_salt
      t.string :encrypted_account_number_iv
      t.integer :country
      t.string :state
      t.string :city
      t.string :address
      t.string :zip

      t.timestamps null: false
    end
  end
end
