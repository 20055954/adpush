class CreateBills < ActiveRecord::Migration
  def change
    create_table :bills do |t|
      t.integer :user_id
      t.float :amount
      t.boolean :paid, :default => false

      t.timestamps null: false
    end
  end
end
