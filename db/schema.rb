# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160117154721) do

  create_table "bank_infos", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "encrypted_code"
    t.string   "encrypted_code_salt"
    t.string   "encrypted_code_iv"
    t.string   "encrypted_account_number"
    t.string   "encrypted_account_number_salt"
    t.string   "encrypted_account_number_iv"
    t.integer  "country"
    t.string   "state"
    t.string   "city"
    t.string   "address"
    t.string   "zip"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "bills", force: :cascade do |t|
    t.integer  "user_id"
    t.float    "amount"
    t.boolean  "paid",       default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "category"
    t.text     "text"
    t.boolean  "addressed",  default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "invoices", force: :cascade do |t|
    t.integer  "bank_acc_id"
    t.float    "amount"
    t.date     "payment_date"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "payment_cards", force: :cascade do |t|
    t.string   "card_num"
    t.integer  "expiry_date_mm"
    t.integer  "expiry_date_yy"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "title"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "products", ["title"], name: "index_products_on_title", unique: true

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true

  create_table "users", force: :cascade do |t|
    t.string   "login"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "email"
    t.string   "email_verification_key"
    t.integer  "utype"
    t.string   "name"
    t.string   "company"
    t.string   "gender"
    t.date     "dob"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.string   "address"
    t.string   "zip"
    t.boolean  "banned",                 default: false
    t.float    "balance",                default: 0.0
    t.integer  "font_size",              default: 0
    t.integer  "color_scheme",           default: 0
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

end
