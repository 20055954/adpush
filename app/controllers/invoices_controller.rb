class InvoicesController < ApplicationController

	def withdraw_revenue
		@wa = params[:amount_to_withdraw][1..params[:amount_to_withdraw].length].to_f
		if current_user.balance >= @wa
			current_user.update_attribute(:balance, current_user.balance - @wa)
			@inv = Invoice.new(bank_acc_id: params[:bank_acc], amount: @wa, payment_date: nil)
			@inv.save
			flash[:success] = "Funds Successfully Withdrawn! The Payment Will Be Carried Out Soon"
			redirect_to(successful_withdrawal_path(:invoice => @inv))
		else
			flash[:error] = "Insufficient Funds"
			redirect_to current_user
		end
	end

	def successful_withdrawal
		@invoice = Invoice.find(params[:invoice])
		@user = current_user
		if BankInfo.find(@invoice.bank_acc_id).user_id != @user.id
			redirect_to root_path
		end
	end

	def generate_invoice_pdf
			@invoice = Invoice.find(params[:invoice])
			pdf = Prawn::Document.new
			pdf.stroke_horizontal_rule
			pdf.pad(10) { pdf.text "AdPush", :align => :left}
			pdf.text "Invoice / " + Date.today.to_s, :align => :left
			pdf.stroke_horizontal_rule
			pdf.move_cursor_to 710
			pdf.text "Random Street 40, Waterford, Co. Waterford, Ireland", :align => :right
			pdf.move_cursor_to 650
			pdf.stroke_horizontal_rule 
			pdf.pad(10) { pdf.text current_user.name, :align => :left }
			pdf.move_cursor_to pdf.cursor+25
			pdf.text "To Be Received", :align => :right
			pdf.text "Upon Receipt", :align => :right
			pdf.text "$" + @invoice.amount.to_s, :align => :right
			pdf.move_cursor_to pdf.cursor+10
			pdf.bounding_box([0,pdf.cursor],:width=>200) do
				pdf.text current_user.address + ", " + current_user.state + ", " + current_user.country, :align => :left 
			end
			pdf.stroke_horizontal_rule 
			pdf.move_cursor_to pdf.cursor-20
			pdf.table([["Item Description", "Quantity", "Price Per Item", "Total"], ["Revenue Withdrawal (Bank Account: " + BankInfo.find(@invoice.bank_acc_id).account_number + ")","1","$" + @invoice.amount.to_s,"$" + @invoice.amount.to_s]])
			pdf.move_cursor_to 20
			pdf.text "AdPush©, " + Date.today.year.to_s, :align => :center
			send_data pdf.render, filename: 'invoice.pdf', type: 'application/pdf'
	end

end
  
