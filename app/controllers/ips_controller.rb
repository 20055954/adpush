# IP data per machine - used for scam prevention
class IpsController < ApplicationController
  
  def new
    @ip = Ip.new
  end

  def create
    @ip = Ip.new(ip_params)
  end

  def update
    @ip.update(ip_params)
  end

  def destroy
    @ip.destroy
  end
  
private
    # Use callbacks to share common setup or constraints between actions.
    def set_ip
      @ip = Ip.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ip_params
      params.require(:ip).permit(:ad, :session, :ip, :date)
    end
end
