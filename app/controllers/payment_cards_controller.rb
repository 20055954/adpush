class PaymentCardsController < ApplicationController
	
	def new
		restrict_access
		
		begin
			restrict_non_verified_access
		rescue
			redirect_to root_path
		end
		
		@from_top_up = params[:top_up]
		@from_bill_pay = params[:bill_pay]
		@payment_card = PaymentCard.new
	end
	
	def create
		@from_top_up = params[:top_up]
		@from_bill_pay = params[:bill_pay]
		secure_params = params.require(:payment_card).permit(:card_num, :expiry_date_mm, :expiry_date_yy, :user_id)
		secure_params[:user_id] = current_user.id

		@payment_card = PaymentCard.new(secure_params) 

		if @payment_card.save
			flash[:success]="Payment Card Added"

			if @from_top_up == 'true'
			redirect_to top_up_path
			elsif @from_bill_pay == 'true'
			redirect_to pay_bills_path
			else
			redirect_to current_user
		end
		else
			render 'new'
		end
	end
	
end
  
  
