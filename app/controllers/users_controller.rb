class UsersController < ApplicationController

	def new
		# Check if someone logged in
		if current_user != nil
			current_user.id 
			flash[:success] = "You Are Already Registered"
			redirect_to root_path
		else
			@user = User.new
		end
	end

	def show
		restrict_access
		@user = User.find(params[:id])
		if current_user.id != @user.id
			redirect_to root_path
		end
		begin
			@ads = Ad.where(:publisher => @user.id)
		rescue
		end
		if Bill.where("created_at < ?", Time.mktime(Date.today.year, Date.today.month)).where(paid: false).where(user_id: @user.id).length > 0
			flash.now[:error] = "Bills For Previous Months Unpaid - Published Ads Won't Be Served"
		end
	end

	def create
		secure_params = params.require(:user).permit(:login, 
		:email, :email_verification_key, :utype, :name, :company, :gender, :dob,
		:country, :state, :city, :address, :zip,
		:password, :password_confirmation)
		secure_params[:gender] = secure_params[:gender].downcase[0]
		secure_params[:email_verification_key] = SecureRandom.random_number(36**12).to_s(36).rjust(12, "0")
		secure_params[:login] = secure_params[:login].downcase.strip 
		secure_params[:email] = secure_params[:email].downcase
		secure_params[:name] = secure_params[:name].strip.split.map(&:capitalize).join(' ')
		secure_params[:company] = secure_params[:company].strip.split.map(&:capitalize).join(' ')
		secure_params[:state] = secure_params[:state].strip.capitalize
		secure_params[:city] = secure_params[:city].strip.capitalize
		secure_params[:address] = secure_params[:address].strip.capitalize
		
		@user = User.new(secure_params)
		
		if @user.save  
			if @user.utype == 0
				Bill.new(user_id: @user.id, amount: 0, paid: false).save
			end
			remember @user     
			flash[:success] = "Welcome to AdPush!"  
			send_verification_email(@user)
		else
			render 'new' 
		end
	end

	def edit_preferences
		extra_protection
	end

	def edit_personal_details
		extra_protection
	end

	def update
		@user = User.find(params[:id])
		
		if params[:email_update] == "y"
			secure_params = params.require(:user).permit(:email)
			secure_params[:email] = secure_params[:email].downcase
			
			unique_email = true
			
			User.all.each do |u|
				if u.email == secure_params[:email]
					unique_email = false
				end
			end
			
			if unique_email
				@user.update_attribute(:email, secure_params[:email])
				@user.update_attribute(:email_verification_key, SecureRandom.random_number(36**12).to_s(36).rjust(12, "0"))
				flash[:changed] = "Email Successfully Changed"
				send_verification_email(@user)
			else
				flash[:error] = "Email Already Registered"
				redirect_to @user
			end

		elsif params[:prefs_update] == "y"
			secure_params = params.require(:user).permit(:color_scheme, :font_size)
			@user.update_attribute(:color_scheme, secure_params[:color_scheme])
			@user.update_attribute(:font_size, secure_params[:font_size])
			flash[:success] = "Preferences Updated"
			redirect_to preferences_path

		else
			old_password = params.permit(:old_password)

			if @user and @user.authenticate(old_password[:old_password]) 
				old_email = @user.email
				secure_params = params.require(:user).permit(:name, :gender, :dob, :country, :state, :city, :zip, :address, :email, :password, :password_confirmation)
		
				@user.update_attributes(secure_params)
				if @user.save  
					remember @user     
					flash[:changed] = "Personal Details Updated"  
					if old_email != @user.email	
						send_verification_email(@user)
					end
					remember @user     
					begin
						redirect_to @user
					rescue
					end
				else
					render 'edit_personal_details'
				end
			else
				flash[:error] = "Invalid Password"
				render 'edit_personal_details'
			end
		end
	end

	def send_verification_email(user)
		@user = user
		begin
			EmailVerificationMailer.verification_email(@user).deliver
		rescue
			@user.update_attribute(:email_verification_key, SecureRandom.random_number(36**12).to_s(36).rjust(12, "0"))
			EmailVerificationMailer.verification_email(@user).deliver
		end
		flash[:success] = "Verification Email Sent" 
		redirect_to @user
	end

	def resend_verification_email
		@user = User.find(params[:user_id])
		EmailVerificationMailer.verification_email(@user).deliver
		flash[:success] = "Repeat Verification Email Sent" 
		redirect_to @user
	end

	def verify_email
		begin
			@user = User.find_by_email_verification_key(params[:email_verification_key])
			@user.update_attribute(:email_verification_key, nil)
			redirect_to verification_success_path(user_id: @user.id)
		rescue
			redirect_to root_path
		end
	end

	def inc_font
		if (current_user.font_size < 2)
			current_user.update_attribute(:font_size, current_user.font_size + 1)
		end
	end

	def dec_font
		if (current_user.font_size != 0)
			current_user.update_attribute(:font_size, current_user.font_size - 1)
		end
	end

	def withdraw_revenue
		restrict_access
		
		begin
			restrict_non_verified_access
		rescue
			redirect_to root_path
		end
		
		if current_user.utype == 0 or current_user.balance < 25
			redirect_to root_path
		end
		
		@user = current_user
		@invoice = Invoice.new 
	end

	# Render top up page
	def top_up
		restrict_access
		
		begin
			restrict_non_verified_access
		rescue
			redirect_to root_path
		end
		
		if current_user.utype == 1
			redirect_to root_path
		end
		
		@user = current_user
	end

	# Top up
	def top_up_action
		# Due to the study nature of the project, no interaction was carried out with the actual banking services.
		restrict_access
		@top_up = params[:top_up_amount][1..params[:top_up_amount].length].to_f
		current_user.update_attribute(:balance, current_user.balance + @top_up)
		flash[:success]="Successful Top Up!"
		redirect_to current_user
	end

	# Render bill pay page
	def pay_bills
		restrict_access
		@user = current_user
	end

	# Pay bills
	def pay_bills_action
		# Due to the study nature of the project, no interaction was carried out with the actual banking services.
		restrict_access
		@bill = Bill.find(params[:bill])
		
		if @bill.user_id == current_user.id
			redirect_to bill_pay_invoice_path(bill_id: @bill.id)
		else
			redirect_to root_path
		end
	end

	# Bill pay invoice page
	def bill_pay_invoice
		restrict_access
		@user = current_user
		@bill = Bill.find(params[:bill_id])
		
		if @bill.paid == true 
			redirect_to current_user
		else
			if @user.id != @bill.user_id
				redirect_to root_path
			else
				@bill.update_attribute(:paid, true)
			end
		end
	end

	def generate_bill_pay_invoice_pdf
		@bill = Bill.find(params[:bill])   
		pdf = Prawn::Document.new
		pdf.stroke_horizontal_rule
		pdf.pad(10) { pdf.text "AdPush", :align => :left}
		pdf.text "Invoice / " + Date.today.to_s, :align => :left
		pdf.stroke_horizontal_rule
		pdf.move_cursor_to 710
		pdf.text "Random Street 40, Waterford, Co. Waterford, Ireland", :align => :right
		pdf.move_cursor_to 650
		pdf.stroke_horizontal_rule 
		pdf.pad(10) { pdf.text current_user.name, :align => :left }
		pdf.move_cursor_to pdf.cursor+25
		pdf.text "Balance Due", :align => :right
		pdf.text "Upon Receipt", :align => :right
		pdf.text "$" + @bill.amount.to_s, :align => :right
		pdf.move_cursor_to pdf.cursor+10
		pdf.bounding_box([0,pdf.cursor],:width=>200) do
			pdf.text current_user.address + ", " + current_user.state + ", " + current_user.country, :align => :left 
		end
		pdf.stroke_horizontal_rule 
		pdf.move_cursor_to pdf.cursor-20
		pdf.table([["Item Description", "Quantity", "Price Per Item", "Total"], ["Bill Payment For " + @bill.created_at_label + " Time Period", "1","$" + @bill.amount.to_s,"$" + @bill.amount.to_s]])
		pdf.move_cursor_to 20
		pdf.text "AdPush©, " + Date.today.year.to_s, :align => :center
		send_data pdf.render, filename: 'invoice.pdf', type: 'application/pdf'
	end

	def performance_analysis
		restrict_access
		
		begin
			restrict_non_verified_access
		rescue
			redirect_to root_path
		end
	end

private

	def extra_protection
		restrict_access
		if current_user != nil
			@user = User.find(current_user.id)
			if current_user.id != @user.id
				redirect_to root_path
			end
			restrict_non_verified_access
		end
	end

end
