class TagsController < ApplicationController

	def show
		restrict_access
		restrict_non_verified_access
		@tag = Tag.find(params[:id])

		@ads = []

		Ad.all.each do |a|
			a.tags.each do |t|
				if t == @tag.name
					@ads.push(a)
				end
			end
		end

		if current_user.utype == 0
			@ads = @ads.delete_if { |a| a.publisher != current_user.id}
		end
	end

	def list_tags
		restrict_access
		restrict_non_verified_access
		
		if current_user != nil
			if current_user.utype == 1
				@tags = Tag.all
			else
				@tags = []
				
				Ad.all.each do |a|
					if a.publisher == current_user.id
						a.tags.each do |t|
							@tags.push(Tag.find_by_name(t.strip))
						end
					end
				end
				
				@tags = @tags.uniq

			end
		else
			redirect_to root_path
		end
	end
	
	# Blocking tags
	def block_tags_action
		restrict_access
		
		@update_values = []
		
		prev_num = 0
		
		params[:products].each do |p|
			for i in prev_num..params[:total_tags_num].to_i-1
				@update_values.push([p, params[:tags][i]])
			end
			prev_num = params[:total_tags_num].to_i - 1
			params[:total_tags_num] = (params[:total_tags_num].to_i*2) - 1
		end
		
		# Replace product names with IDs and tag names with IDs (? ¬ = true => unblock)
		@update_values.each do |upd|
			upd[0] = Product.find_by_title(upd[0]).id
			tag_to_find = upd[1].split("¬")[0]
			upd[1] = [Tag.find_by_name(tag_to_find[5..tag_to_find.length - 6]).id, upd[1].include?("¬")]
		end
		
		Ad.all.each do |ad|
			if ad.blocked_tags != nil
				ad.tags.each do |t|
					@update_values.each do |upd|
						if upd[1][0] == Tag.find_by_name(t).id and upd[1][1] != true
							ad.update_attribute(:blocked_tags, ad.blocked_tags.push([upd[0], upd[1][0]]))
						elsif upd[1][0] == Tag.find_by_name(t).id and upd[1][1] == true
							ad.update_attribute(:blocked_tags, ad.blocked_tags.delete_if {|btd| btd[1] == upd[1][0] and btd[0] == upd[0] })
						end
					end
				end
			else
				@to_insert = []
				ad.tags.each do |t|
					@update_values.each do |upd|
						begin
							if upd[1][0] == Tag.find_by_name(t).id and upd[1][1] != true
								@to_insert.push([upd[0], upd[1][0]])
							end
						rescue
						end
					end
				end
				ad.update_attribute(:blocked_tags, @to_insert)
			end
		end
		
		Ad.all.each do |ad|
			ad.update_attribute(:blocked_tags, ad.blocked_tags.uniq)
		end
		
		flash[:success] = "Block Settings Updated"
		render :js => "window.location = '#{block_tags_path}'" 
	end
	
	# Rendering tags blocking page
	def block_tags
		restrict_access
		restrict_non_verified_access
		if current_user.utype == 0
			redirect_to root_path
		end
		
		if params[:products] != nil
			cookies[:products] = { value: params[:products], expires: 1.hour.from_now }
		end
		
		if params[:products] != nil
			render :js => "window.location = '#{block_tags_path}'" 
		end
	end
	
end
