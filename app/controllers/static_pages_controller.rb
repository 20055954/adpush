class StaticPagesController < ApplicationController

  def home
	if current_user != nil
		if current_user.banned == false
			if cookies[:first_session_visit] == nil
				cookies[:first_session_visit] = { value: false, expires: 2.hours.from_now }
				flash[:success] = "Welcome Back!"
				redirect_to current_user
			else
				redirect_to current_user
			end
		else
		end
	end
  end

  def uploaded
  end
  
  def verification_success
  end
  
  def terms_and_conditions
  end
  
  def api
  end

end
