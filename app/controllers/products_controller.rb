class ProductsController < ApplicationController

	def new
		@product = Product.new
	end

	def index
		restrict_access
		if current_user.utype == 0
			redirect_to root_path
		end
		@products = Product.where(:user_id => current_user.id)
	end

	def show
		redirect_to root_path
	end

	def create
		secure_params = params.require(:product).permit(:title, :user_id)
		secure_params[:title] = secure_params[:title] + "¬"
		secure_params[:user_id] = current_user.id
		@product = Product.new(secure_params)
		
		begin
			if @product.save   
				flash[:success] = "New Software Product Added"
				redirect_to products_path
			else
				render 'new' 
			end
		rescue
			@product.errors.add(:title, "must be unique")
			render 'new'
		end
	end

	def destroy
		params[:products].each do |p|
			@product_id = Product.find_by_title(p[3..p.length - 4]).id
			Product.where(:user_id => current_user.id).destroy_all(:title => p[3..p.length - 4])
			
			Ad.all.each do |ad|
				if ad.blocked_tags != nil
					ad.update_attribute(:blocked_tags, ad.blocked_tags.delete_if {|bt| bt[0] == @product_id})
				end
			end
		end
		flash[:success] = "Requested Products Deleted"
		render :js => "window.location = '#{products_path}'" 
	end

end
