class AdsController < ApplicationController
	include ApplicationHelper
	before_action :set_ad, only: [:show, :edit, :update, :destroy]

	def show
		restrict_access
		restrict_non_verified_access
		@ad = Ad.find(params[:id])
		@user = User.find(@ad.publisher)
		if current_user.id != @user.id and current_user.utype == 0
			redirect_to root_path
		end
	end

	def new
		restrict_access

		begin
			restrict_non_verified_access
		rescue
		end

		if current_user.utype != 0
			redirect_to root_path
		end

		@ad = Ad.new
	end

	def edit
		if @ad.publisher == current_user.id
			@ad = Ad.find(params[:id]);
		else
			redirect_to root_path
		end
	end

	def update
			@ad = Ad.find(params[:id])
			
			@remove_image = false;
			if (params[:remove_image] != nil)
				@remove_image = true;
			end
			secure_params = params.require(:ad).permit(:link, :desc, :min_age, :max_age, :countries, :content, :c_vals)
			if secure_params[:content] == nil
				secure_params[:content] = @ad.content
			end
			
			if @remove_image == true
				secure_params[:content] = nil
			end
			@c_list = params[:c_vals]
			@countries_sorted = @c_list.split(",").sort_by{ |c|  c.downcase}
			@country_codes = Array.new

			@countries_sorted.each do |c|
			index = 0
			countries_list.each do |compare|
				if compare == c
					@country_codes.push(index)
				end
				index = index + 1
			end
		end

		secure_params[:countries] = @country_codes.uniq
		if @ad.update(secure_params)
			flash[:success] = "Ad Updated"
			@ad.update_attribute(:impressions, nil)
			@ad.update_attribute(:clicks, nil)
			redirect_to @ad
		else
			render 'edit'
		end
	end

	def create
		@c_list = params[:c_vals]
		secure_params = params.require(:ad).permit(:desc, :link, :tags, :countries, :min_age, :max_age, :content, :publisher, :c_vals)

		secure_params[:publisher] = current_user.id

		begin
			img_check = Magick::ImageList.new(secure_params[:content].tempfile.path)

			if img_check.scene == 0
				secure_params[:animation] = false
			else
				secure_params[:animation] = true
			end
		rescue
		end

		begin
			secure_params[:content] = Image.new(secure_params[:content])
		rescue
		end

		@countries_sorted = @c_list.split(",").sort_by{ |c|  c.downcase}
		@country_codes = Array.new

		@countries_sorted.each do |c|
			index = 0
			countries_list.each do |compare|
				if compare == c
					@country_codes.push(index)
				end
				index = index + 1
			end
		end

		secure_params[:countries] = @country_codes.uniq

		@tags = secure_params[:tags].downcase.split(",").collect {|t| t.strip || t}

		@tags.each do |t|
		begin
				t = t + "¬"
				if t != "¬"
					Tag.new(name: t).save
				end
		rescue
		end
		end

		 secure_params[:tags] = []
		 
		 @tags.uniq.each do |t|
			if !t.blank?
				secure_params[:tags] = secure_params[:tags].push(t)
			end
		 end

		@ad = Ad.new(secure_params) 

		# Update blocked tags in products
		Ad.all.each do |ad|
			if ad.blocked_tags != nil
				ad.blocked_tags.each do |bt|
					@ad.tags.each do |t|
						begin
							if t == Tag.find(bt[1]).name
								if @ad.blocked_tags == nil
									@ad.blocked_tags = [bt[0], bt[1]]
								else
									@ad.blocked_tags = @ad.blocked_tags.push([bt[0], bt[1]])
								end
							end
						rescue
						end
					end
				end
			end
		end

		if @ad.save
			flash[:success]="New Ad Succesfully Uploaded!"
			redirect_to @ad
		  else
			render 'new'
		  end

		end

	def destroy
		if current_user.id == @ad.publisher
			@ad.destroy
			flash[:success]="Ad Successfully Deleted"
			redirect_to current_user
		else
			redirect_to root_path
		end
	end

	def search
		cookies[:search_request] = { value: true, expires: 1.minute.from_now }
		cookies[:search_query] = { value: params[:search], expires: 1.minute.from_now }
		cookies[:show_images] = { value: params[:show_images], expires: 1.minute.from_now }
		redirect_to current_user
	end

	# Block ad in specific product
	def single_ad_block
		restrict_access
		
		@ad = Ad.find(params[:ad_id])
		@upd_block = []
		
		if @ad.blocked_tags != nil
			@ad.blocked_tags.each do |bt|
				current_user.products.each do |cp|
					if bt[0] != cp.id and bt[1].is_a? Numeric
						@upd_block.push(bt)
					end
				end		
			end
		end
		
		if params[:products] != nil
			params[:products].each do |upd|
				@upd_block.push([Product.find_by_title(upd[3..upd.length-2]).id, "x"])
			end
		end
		
		@ad.update_attribute(:blocked_tags, @upd_block)

		flash[:success] = "Ad " + @ad.id + " Block Parameters Modified"
		render :js => "window.location.reload();"
	end
	  
	# nil is returned if country or scam validations are not passed
	# Retrieve single ad by its ID
	def get_ad
		begin
			@product = Product.find_by_title(params[:product].downcase.capitalize.strip)
			@dev = User.find(@product.user_id)
			@ad = Ad.find(params[:ad_id])
			
			# Check if the previous month bill was paid		
			@cont = true
			begin
				if Bill.where("created_at < ?", Time.mktime(Date.today.year, Date.today.month)).where(paid: false).where(user_id: @dev.id).length > 0
					@cont = false
				end
			rescue
			end
			
			if @cont == true
				if restrict_country(@ad.countries) == false
					if params[:age] != nil and params[:age].to_i >= @ad.min_age and params[:age].to_i <= @ad.max_age
						@return = [@ad.link, @ad.desc, @ad.content.url, @ad.animation]
					elsif params[:age] == nil
						@return = [@ad.link, @ad.desc, @ad.content.url, @ad.animation]	
					end
				end
				
				begin
							@ad.blocked_tags.each do |bt|
								if bt[0] == @product.id
									@return = nil
									break
								end
							end
				rescue
				end

				render :json => @return
				if check_if_scam(@ad.id, false) == false and @return != nil
					@dev.update_attribute(:balance, @dev.balance + 0.00036)
					@geoip ||= GeoIP.new("./db/GeoIP.dat")    
					if @ad.impressions != nil
						@imp = @ad.impressions.push([@product.id, request.remote_ip, @geoip.country(request.remote_ip).country_name, Date.today.to_s, params[:age], params[:gender]])
					else
						@imp = [[@product.id, request.remote_ip, @geoip.country(request.remote_ip).country_name, Date.today.to_s, params[:age], params[:gender]]]
					end
					@ad.update_attribute(:impressions, @imp)
					
										@publisher = User.find(@ad.publisher)
						if @publisher.balance >= 0.0006
							@publisher.update_attribute(:balance, @publisher.balance - 0.0006)
						else
							begin
								@publisher_bill = Bill.where("created_at >= ? and created_at < ?", Time.mktime(Date.today.year, Date.today.month), Time.mktime(Date.today.year, Date.today.month + 1)).find_by_user_id(@ad.publisher)
								@publisher_bill.update_attribute(:amount, @publisher_bill.amount + 0.0007)
							rescue
								Bill.new(user_id: @ad.publisher, amount: 0.0007, paid: false).save
							end
						end
				end
			else
				render :json => "Bill Not Paid"
			end
		rescue
			render :json => "Invalid Product / Ad"
		end
	end

	# Retrieve random ad
	def get_random_ad
		begin
			@product = Product.find_by_title(params[:product].downcase.capitalize.strip)
			@dev = User.find(@product.user_id)
			@ad = Ad.offset(rand(Ad.count)).first
			
			until Bill.where("created_at < ?", Time.mktime(Date.today.year, Date.today.month)).where(paid: false).where(user_id: @dev.id).length == 0
				  @ad = Ad.offset(rand(Ad.count)).first
			end
			
			@tag_blocked = true
			@all_ads = Ad.pluck(:id)
					
			until @tag_blocked == false or @all_ads.length == 0
						begin
							@ad.blocked_tags.each do |bt|
								if bt[0] == @product.id
									@tag_blocked = true
									break
								else
									@tag_blocked = false
								end
							end
						rescue
							@tag_blocked = false
						end
						
						if @tag_blocked == true or restrict_country(@ad.countries) == true
							@all_ads = @all_ads - [@ad.id]
							@ad = Ad.offset(rand(Ad.count)).first
						end
						
						if params[:age] != nil and params[:age].to_i < @ad.min_age or params[:age] != nil and  params[:age].to_i > @ad.max_age
							@all_ads = @all_ads - [@ad.id]
							@ad = Ad.offset(rand(Ad.count)).first
						end
			end
			
			
			if restrict_country(@ad.countries) == false
				if params[:age] != nil and params[:age].to_i >= @ad.min_age and params[:age].to_i <= @ad.max_age
					@return = [@ad.link, @ad.desc, @ad.content.url, @ad.animation, @ad.id.to_s]
				elsif params[:age] == nil
					@return = [@ad.link, @ad.desc, @ad.content.url, @ad.animation, @ad.id.to_s]	
				end
			else
				@return = nil
			end
			
			if @tag_blocked == true
				@return = nil
			end
			
			render :json => @return
			
			if check_if_scam(@ad.id, false) == false and @return != nil
			
				@dev.update_attribute(:balance, @dev.balance + 0.00036)
				@geoip ||= GeoIP.new("./db/GeoIP.dat")    
				if @ad.impressions != nil
					@imp = @ad.impressions.push([@product.id, request.remote_ip, @geoip.country(request.remote_ip).country_name, Date.today.to_s, params[:age], params[:gender]])
				else
					@imp = [[@product.id, request.remote_ip, @geoip.country(request.remote_ip).country_name, Date.today.to_s, params[:age], params[:gender]]]
				end
				@ad.update_attribute(:impressions, @imp)
				
				@publisher = User.find(@ad.publisher)
				
				if @publisher.balance >= 0.0006
					@publisher.update_attribute(:balance, @publisher.balance - 0.0006)
				else
					begin
						@publisher_bill = Bill.where("created_at >= ? and created_at < ?", Time.mktime(Date.today.year, Date.today.month), Time.mktime(Date.today.year, Date.today.month + 1)).find_by_user_id(@ad.publisher)
						@publisher_bill.update_attribute(:amount, @publisher_bill.amount + 0.0007)
					rescue
						Bill.new(user_id: @ad.publisher, amount: 0.0007, paid: false).save
					end
				end
				
			end
		rescue
			render :json => "Invalid Product"
		end
	end

	# Retrieve random ad described with the specified tags
	def get_random_ad_by_tags
		begin
			@product = Product.find_by_title(params[:product].downcase.capitalize.strip)
			@dev = User.find(@product.user_id)
			@tags_ids = []
			@ads = []
			Ad.all.each do |ad|
				params[:tags].split(",").each do |t|
					@tags_ids.push Tag.find_by_name(t).id
					ad.tags.each do |at|
						if at == t.strip.downcase
							if restrict_country(ad.countries) == false
								@ads.push(ad)
							end
						end
					end
				end
			end
			
			@all_ads_non_verified = @ads.uniq
			@all_ads = []
			
			@all_ads_non_verified.each do |ad|
				begin
				if Bill.where("created_at < ?", Time.mktime(Date.today.year, Date.today.month)).where(paid: false).where(user_id: @dev.id).length == 0
					@all_ads.push ad
				end
				# First Month
				rescue
					@all_ads.push ad
				end
			end
		
			@ad = nil
			@return = nil
			@all_ads = @all_ads.shuffle
			
			until @return != nil or @all_ads.length == 0
				@ad = nil
				@blocked = false

				if @all_ads[0].blocked_tags != nil
					@all_ads[0].blocked_tags.each do |bt|
						@tags_ids.each do |ti|
									if bt[1] == ti and bt[0] == @product.id
										@blocked = true
									elsif bt[1] == 'x' and bt[0] == @product.id
									end
						end			
						
						if @blocked == false
							tag_included = params[:tags].split(",").length # When 0 - Then Pass
							params[:tags].split(",").each do |t|
								@all_ads[0].tags.each do |at|
									if at == t
										tag_included = tag_included - 1
									end
								end
							end
							
							if tag_included != 0 or restrict_country(@all_ads[0].countries) == true
								@ad = 0 # 0 Means Validation Is Not Passed
							end		
							
							if params[:age] != nil and params[:age].to_i < @all_ads[0].min_age.to_i
								@ad = 0
							elsif params[:age] != nil and params[:age].to_i > @all_ads[0].max_age.to_i
								@ad = 0
							end									
						end
					end
				else
					tag_included = params[:tags].split(",").length # When 0 - Then Pass
					params[:tags].split(",").each do |t|
						@all_ads[0].tags.each do |at|
							if at == t
								tag_included = tag_included - 1
							end
						end
					end
					
					if tag_included != 0 or restrict_country(@all_ads[0].countries) == true
						@ad = 0
					end		
					
					if params[:age] != nil and params[:age].to_i < @all_ads[0].min_age.to_i
						@ad = 0
					elsif params[:age] != nil and params[:age].to_i > @all_ads[0].max_age.to_i
						@ad = 0
					end	
				end
	
				if @blocked != true and @ad == nil
					@return = [@all_ads[0].link, @all_ads[0].desc, @all_ads[0].content.url, @all_ads[0].animation, @all_ads[0].id.to_s]
				end
				
				@all_ads.pop
			end

			if @return == nil
			   @return = "Nothing Was Found"
			end
				
			render :json => @return
			
			if check_if_scam(@return[4], false) == false and @return != "Nothing Was Found"
				@ad = Ad.find(@return[4])
				@dev.update_attribute(:balance, @dev.balance + 0.00036)
				@geoip ||= GeoIP.new("./db/GeoIP.dat")    
				if @ad.impressions != nil
					@imp = @ad.impressions.push([@product.id, request.remote_ip, @geoip.country(request.remote_ip).country_name, Date.today.to_s, params[:age], params[:gender]])
				else
					@imp = [[@product.id, request.remote_ip, @geoip.country(request.remote_ip).country_name, Date.today.to_s, params[:age], params[:gender]]]
				end
				@ad.update_attribute(:impressions, @imp)
				
				@publisher = User.find(@ad.publisher)
				if @publisher.balance >= 0.0006
					@publisher.update_attribute(:balance, @publisher.balance - 0.0006)
				else
					begin
						@publisher_bill = Bill.where("created_at >= ? and created_at < ?", Time.mktime(Date.today.year, Date.today.month), Time.mktime(Date.today.year, Date.today.month + 1)).find_by_user_id(@ad.publisher)
						@publisher_bill.update_attribute(:amount, @publisher_bill.amount + 0.0007)
					rescue
						Bill.new(user_id: @ad.publisher, amount: 0.0007, paid: false).save
					end
				end
			end
		rescue
			begin
				render :json => "Invalid Product"
			rescue # Bug Prevention
			end
		end
	end

	# Retrieve ad using recommender algorithm
	def suggested_ad
		@product = params[:product]
			
		# To ensure that the user sees new ads
		if rand(1..10) > 1
			# Ensure That Some Ad Is Still Served When The Database Is Being Updated (See schedule.rb)
			begin
				@geoip ||= GeoIP.new("./db/GeoIP.dat") 	
				age = params[:age].to_i
				gender = params[:gender].upcase
				country = @geoip.country(request.remote_ip).country_name
				
				# Specified Age, Specified Country, Specified Gender
				@suggestion = SuggestionTable.where(age: age, gender: gender, country: country, quadrant: false, decade: false)
				
				# Specified Age, Specified Country, Nil Gender
				if @suggestion == nil
					@suggestion = SuggestionTable.where(age: age, gender: nil, country: country, quadrant: false, decade: false)
				end
				
				# Specified Age Decade, Specified Country, Specified Gender
				if @suggestion == nil
					@suggestion = SuggestionTable.where(age: age, gender: gender, country: country, quadrant: false, decade: true)
				end
				
				# Specified Age Decade, Specified Country, Nil Gender
				if @suggestion == nil
					@suggestion = SuggestionTable.where(age: age, gender: nil, country: country, quadrant: false, decade: true)
				end
				
				# Specified Age Quadrant, Specified Country, Specified Gender		
				if @suggestion == nil
					@suggestion = SuggestionTable.where(age: age, gender: gender, country: country, quadrant: true, decade: false)
				end

				# Specified Age Quadrant, Specified Country, Nil Gender			
				if @suggestion == nil
					@suggestion = SuggestionTable.where(age: age, gender: nil, country: country, quadrant: true, decade: false)
				end	
				
				# Specified Age, N/A Country, Specified Gender
				if @suggestion == nil
					@suggestion = SuggestionTable.where(age: age, gender: gender, country: 'N/A', quadrant: false, decade: false)
				end	
					
				# Specified Age, N/A Country, Nil Gender
				if @suggestion == nil
					@suggestion = SuggestionTable.where(age: age, gender: nil, country: 'N/A', quadrant: false, decade: false)
				end		
				
				if @suggestion == nil
					redirect_to random_ad_path(request.parameters.merge(product: @product, age: age, gender: gender))
				else
					if @suggestion[0].fav_ads.length != 10
						# Random Ad By One Of The Top Tags Being Served Chance
						if rand(1..10) < 3
						    ad_to_get_tags = Ad.find(@suggestion[0].fav_ads.sample[0])
						    tag_in = ad_to_get_tags.tags.sample
							redirect_to random_ad_tags_path(request.parameters.merge(product: @product, tags: tag_in, age: age, gender: gender))
						else 
							@suggestion = @suggestion[0].fav_ads.sample[0]
							redirect_to single_ad_path(request.parameters.merge(product: @product, ad_id: @suggestion))
						end
					else
						@suggestion = @suggestion[0].fav_ads.sample[0]
						redirect_to single_ad_path(request.parameters.merge(product: @product, ad_id: @suggestion))
					end
				end
				
			rescue 
					# For JAVA API (HttpRequest does not support redirect)
					render :json => "http://adpush.herokuapp.com/random_ad"
			end
		else
			begin
				 redirect_to random_ad_path(request.parameters.merge(product: @product, age: age, gender: gender))
			rescue
				 render :json => "http://adpush.herokuapp.com/random_ad"
			end
		end
	end

	# Ad clicked by user
	def click_ad
		begin
			@product = Product.find_by_title(params[:product].downcase.capitalize.strip)
			@dev = User.find(@product.user_id)
			@ad = Ad.find(params[:ad_id])
						
			@bill_paid = true
			begin
				if Bill.where("created_at < ?", Time.mktime(Date.today.year, Date.today.month)).where(paid: false).where(user_id: @dev.id).length > 0
					@bill_paid = false
				end
			rescue
			end
			
			@country_age_check == false
			
			if @bill_paid == true
				if restrict_country(@ad.countries) == false
					if params[:age] != nil and params[:age].to_i >= @ad.min_age and params[:age].to_i <= @ad.max_age
						@country_age_check = true
					elsif params[:age] == nil
						@country_age_check = true
					else
						@country_age_check = false
					end
				end
				
				@block_check = true
				begin
							@ad.blocked_tags.each do |bt|
								if bt[0] == @product.id
									@block_check = false
									break
								end
							end
				rescue
				end

				if check_if_scam(@ad.id, false) == false and @country_age_check == true and @block_check == true
					@dev.update_attribute(:balance, @dev.balance + 0.68)
					@geoip ||= GeoIP.new("./db/GeoIP.dat")    
					if @ad.clicks != nil
						@clicks = @ad.clicks.push([Product.find_by_title(params[:product].downcase.capitalize.strip).id, request.remote_ip, @geoip.country(request.remote_ip).country_name, Date.today.to_s, params[:age], params[:gender]])
					else
						@clicks = [[Product.find_by_title(params[:product].downcase.capitalize.strip).id, request.remote_ip, @geoip.country(request.remote_ip).country_name, Date.today.to_s, params[:age], params[:gender]]]
					end
					@ad.update_attribute(:clicks, @clicks)
					
										@publisher = User.find(@ad.publisher)
						if @publisher.balance >= 0.9 
							@publisher.update_attribute(:balance, @publisher.balance - 0.9)
						else
							begin
								@publisher_bill = Bill.where("created_at >= ? and created_at < ?", Time.mktime(Date.today.year, Date.today.month), Time.mktime(Date.today.year, Date.today.month + 1)).find_by_user_id(@ad.publisher)
								@publisher_bill.update_attribute(:amount, @publisher_bill.amount + 1)
							rescue
								Bill.new(user_id: @ad.publisher, amount: 1, paid: false).save # Extra Precaution
							end
						end
				end
				
				if @country_age_check == true and @block_check == true
					redirect_to @ad.link
				else
					render :json => nil
				end
			else
				render :json => "Bill Not Paid"
			end
		rescue
			render :json => "Invalid Product / Ad"
		end
	end
	
private
    # Use callbacks to share common setup or constraints between actions.
    def set_ad
      @ad = Ad.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ad_params
      params.require(:ad).permit(:title, :link)
    end
end
