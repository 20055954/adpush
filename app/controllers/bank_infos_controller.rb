class BankInfosController < ApplicationController

	def new
		restrict_access

		begin
		restrict_non_verified_access
		rescue
		redirect_to root_path
		end

		@from_withdrawal = params[:from_withdrawal]
		@bank_details = BankInfo.new
	end

	def create
		secure_params = params.require(:bank_info).permit(:user_id, :name, :code, :account_number, :country, :city, :state, :zip, :address)
		@fwd = params[:from_withdrawal]
		secure_params[:user_id] = current_user.id
		secure_params[:state] = secure_params[:state].strip.capitalize 
		secure_params[:city] = secure_params[:city].strip.capitalize 
				secure_params[:address] = secure_params[:address].strip.capitalize 
						secure_params[:name] = secure_params[:name].strip.capitalize 

		@bank_details = BankInfo.new(secure_params) 


		if @bank_details.save
		flash[:success]="Bank Account Details Added"

		if @fwd == "true"
			redirect_to withdraw_revenue_path
		else
			redirect_to current_user
		end

		else
			render 'new'
		end
	end
	
end
  
  
