class FeedbacksController < ApplicationController
	
	def new
		restrict_non_verified_access
		begin
		restrict_access_ignore_ban
		rescue # Preventing double redirect if the first check was positive (restrict_non_verified_access)
		end
		@feedback = Feedback.new
	end
	
	def create
		secure_params = params.require(:feedback).permit(:user_id, :category, :text)
		secure_params[:user_id] = current_user.id
		secure_params[:text] = secure_params[:text].strip

		@feedback = Feedback.new(secure_params) 


		if @feedback.save
		flash[:success]="Thank You For Getting In Touch! We Will Contact You Back Soon"
		redirect_to current_user
		else
		render 'new'
		end
	end
	
end
  
  
