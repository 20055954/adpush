class SessionsController < ApplicationController

	def new
		if current_user != nil
			redirect_to root_path
		end
	end

	def create
		params[:session][:login_or_email] = params[:session][:login_or_email].downcase.strip
		user = User.find_by_email(params[:session][:login_or_email])
		if user == nil
			user = User.find_by_login(params[:session][:login_or_email])
		end
		if user && user.authenticate(params[:session][:password])
		  remember user       
		  redirect_to user     
		else 
		  flash.now[:error] = 'Invalid User Credentials'
		  render 'new'
		end
	end

	def destroy
		log_out if logged_in?
	end 

end
