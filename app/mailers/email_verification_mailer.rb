class EmailVerificationMailer < ApplicationMailer
	def verification_email(user)
		@user = user
		@url = "http://adpush.herokuapp.com/verify_email?email_verification_key=" + user.email_verification_key
		mail(to: @user.email, subject: 'AdPush Account Verification')
	end
end
