$(document).ready(function () {
	if ($("#amount_to_withdraw").val().substr(0, 1) != "$")
	{ $("#amount_to_withdraw").val("$" + $("#amount_to_withdraw").val()); }
	$("#amount_to_withdraw").keyup(function () {
		if ($("#amount_to_withdraw").val().substr($("#amount_to_withdraw").val().length - 1) != "." && $("#amount_to_withdraw").val().substr($("#amount_to_withdraw").val().length - 2) != ".0" && $("#amount_to_withdraw").val() != "$")
		{
			$("#amount_to_withdraw").val("$" + parseFloat($("#amount_to_withdraw").val().split("$")[1]));
			if ($("#amount_to_withdraw").val() == "$undefined") { $("#amount_to_withdraw").val("$0"); }
			if (isNaN(parseFloat($("#amount_to_withdraw").val().split("$")[1]))) { $("#amount_to_withdraw").val("$0"); }
		}
		
		if (parseFloat($("#amount_to_withdraw").val().split("$")[1]) < 25 || $("#amount_to_withdraw").val() == "$")
		{ $(".btn.btn-primary.btn-withdraw").attr("disabled", "disabled"); }
		else
		{ $(".btn.btn-primary.btn-withdraw").attr("disabled", false); }
	});
});
