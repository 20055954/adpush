$(document).ready(function () {
	$(".btn").click(function() {
		if ($(this).attr("class").split(" ")[1] == "btn" && $(this).attr("class").split(" ")[2] == "btn-product-selected")
		{ $(this).addClass("btn-product").removeClass("btn-product-selected"); }
		else if ($(this).attr("class").split(" ")[1] == "btn" && $(this).attr("class").split(" ")[2] == "btn-product")
		{ $(this).addClass("btn-product-selected").removeClass("btn-product"); }
		if ($(this).attr("class").split(" ")[1] == "ad-block-btn")
		{ 
			var vals = []
			$("." + $(this).attr("class").split(" ")[3] + ".btn.btn-product-selected").each(function () {
				vals.push($(this).text());
			});
			$.get('/block_ad_in_product', {products: vals, ad_id: $(this).attr("class").split(" ")[3]});
			
			}
	});
});
