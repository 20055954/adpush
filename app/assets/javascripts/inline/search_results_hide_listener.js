$(document).ready(function() {
	$(".btn-hide").click(function() {
		$(".search-results").toggle();
		var show = "Show Search Results";
		var hide = "Hide Search Results";
		if ($(this).text() != show)
		{ $(this).text(show); }
		else
		{ $(this).text(hide); }
	});
});
