$(document).ready(function() {
  if(!Cookies.get('cookie_warning')){
       alert ("This website utilises cookies in order to improve user experience and allow for the proper functioning of the service. By continuing to browse the website, you give your acceptance to the use of cookies.");
	   Cookies.set('cookie_warning', 1);
  }
});
