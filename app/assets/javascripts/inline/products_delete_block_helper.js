$(document).ready(function () {
	$(".btn").click(function() {
		if ($(this).attr("class") == "btn btn-product-selected")
		{ $(this).addClass("btn-product").removeClass("btn-product-selected"); }
		else if ($(this).attr("class") == "btn btn-product")
		{ $(this).addClass("btn-product-selected").removeClass("btn-product"); }
		
		if ($(".btn-product-selected").length != 0) 
		{ 
			$("#delete-btn").attr("disabled", false); 
			$("#block-btn").attr("disabled", false);
		}
		else 
		{ 
			$("#delete-btn").attr("disabled", true);
			$("#block-btn").attr("disabled", true);
		}
	});
	
	$("#delete-btn").click(function() {
		var proceed = confirm("Are you sure?");
		
		if (proceed == true)
		{
			var vals = [];
			$(".btn-product-selected").each(function () {
				vals.push($(this).text());
			});
			$.get('/delete_products', {products: vals});
		}
	});
	
	$("#block-btn").click(function() {
			var vals = [];
			$(".btn-product-selected").each(function () {
				vals.push($(this).text());
			});
			$.get('/block_tags', {products: vals});
	});
});
