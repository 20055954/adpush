$(document).ready(function () {
	$(".btn").click(function() {
		if ($(this).hasClass("btn-tag"))
		{
			if ($(this).hasClass("btn-tag-selected")) { $(this).removeClass("btn-tag-selected"); }
			else { $(this).addClass("btn-tag-selected"); }
		}
	});
	
	$("#block-btn").click(function() {
			var products = [];
			var tags = []; 
			
			$(".selected-product").each(function() {
				products.push($(this).text());
			});
			
			$(products).each(function (index, object) {
				$(".btn-tag-selected").each(function () {
					if ($(this).hasClass(object.split(" ").join("¬")))
					{ tags.push($(this).text().substring(1, $(this).text().length).substring(0, $(this).text().length - 2)); }
				});
				$(".btn-tag").each(function () {
					if ($(this).hasClass(object.split(" ").join("¬")) && $(this).hasClass("btn-tag-selected") == false)
					{ tags.push($(this).text().substring(1, $(this).text().length).substring(0, $(this).text().length - 2) + "¬"); }
				});
			});
			
			$.get('/block', {products: products, tags: tags, total_tags_num: ((tags.length + 1) / (products.length + 1))});
	});
});
