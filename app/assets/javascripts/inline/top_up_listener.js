$(document).ready(function () {
	if ($("#top_up_amount").val().substr(0, 1) != "$")
	{ $("#top_up_amount").val("$" + $("#top_up_amount").val()); }
	
	$("#top_up_amount").keyup(function () {
		if ($("#top_up_amount").val().substr($("#top_up_amount").val().length - 1) != "." && $("#top_up_amount").val().substr($("#top_up_amount").val().length - 2) != ".0" && $("#top_up_amount").val() != "$")
		{
			$("#top_up_amount").val("$" + parseFloat($("#top_up_amount").val().split("$")[1]));
			if ($("#top_up_amount").val() == "$undefined") { $("#top_up_amount").val("$0"); }
			if (isNaN(parseFloat($("#top_up_amount").val().split("$")[1]))) { $("#top_up_amount").val("$0"); }
		}
		
		if (parseFloat($("#top_up_amount").val().split("$")[1]) <= 0 || $("#top_up_amount").val() == "$" || $("#cvv").val().length != 3 || $("#cvv").val().match(/^\d+$/) == false)
		{ $("#top-up-btn").attr("disabled", "disabled"); }
		else
		{ $("#top-up-btn").attr("disabled", false); }
	});
	
	$("#cvv").keyup(function () {
		
		if ($("#cvv").val().length > 3)
		{ $("#cvv").val($("#cvv").val().substr(0, 3));}
		
		if ($("#cvv").val().length == 3 && $("#cvv").val().match(/^\d+$/) && parseFloat($("#top_up_amount").val().split("$")[1]) > 0 && $("#top_up_amount").val() != "$")
		{ $("#top-up-btn").attr("disabled", false); }
		else { $("#top-up-btn").attr("disabled", "disabled"); }
	});
});
