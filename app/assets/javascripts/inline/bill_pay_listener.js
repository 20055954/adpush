$(document).ready(function () {
	$("#cvv").keyup(function () {
		if ($("#cvv").val().length > 3)
		{ $("#cvv").val($("#cvv").val().substr(0, 3));}
		
		if ($("#cvv").val().length == 3 && $("#cvv").val().match(/^\d+$/))
		{ $("#bill-pay-btn").attr("disabled", false); }
		else { $("#bill-pay-btn").attr("disabled", "disabled"); }
	});
});
