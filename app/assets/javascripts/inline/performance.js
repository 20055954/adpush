$(document).ready( function() {
	
	$('[data-toggle="tooltip"]').tooltip(); 
	$('#ads_filter option').prop('selected', true);
	
	imp_by_m();
	
	$("#select_all_ads").click(function () {
		$('#ads_filter option').prop('selected', true);
		upd();
	});
	
	$("#deselect_all_ads").click(function () {
		$('#ads_filter option').prop('selected', false);
		upd();
	});
	
	$("#perf_selector").change(function () {
		upd();
	});
	
	$("#ads_filter").change(function() {
		upd();
	});
});

function imp_by_m()
{
	$("#perf_capt").text("Impressions By Month");
	by_m(".imp-date");
}

function imp_by_y()
{
	$("#perf_capt").text("Impressions By Year");
	by_y(".imp-date");
}

function clicks_by_m()
{
	$("#perf_capt").text("Clicks By Month");
    by_m(".click-date");
}

function clicks_by_y()
{
	$("#perf_capt").text("Clicks By Year");
	by_y(".click-date");
}

function top_countries_by_clicks() {
	$("#perf_capt").text("Top 5 Countries By Clicks");
	top_country(".click-country");
}

function top_countries_by_impressions() {
	$("#perf_capt").text("Top 5 Countries By Impressions");
	top_country(".imp-country");
}

function top_ads_imp() 
{
	$("#perf_capt").text("Top 5 Most Retrieved Ads");
	top_ad(".imp-date");

} 

function top_ads_click() {
	$("#perf_capt").text("Top 5 Most Clicked Ads");
	top_ad(".click-date");
}

function top_tags_imp()
{
	$("#perf_capt").text("Top 5 Most Retrieved Tags");
	$("#top").css("display", "none");
	$("#chart").css("display", "flex");
	
	var tags_by_imp = []
	
	$(".ad-info").each (function (index, object_i) {
		var go = false;
		
		$("#ads_filter option:selected").each(function (index, object) {
			if ($(object).val() == $(object_i).val().split("¬")[1])
			{ 
				go = true;
			}
		});
		
		if (go == true)
		{
			$($(object_i).val().split("¬")[2].split(",")).each (function (index_t, object_t) {
				var tag;
				
				if (index_t != $(".ad-info").val().split("¬")[2].split(",").length - 1)
				{ tag = object_t.substr(2, object_t.length - 3); }
				else
				{ tag = object_t.substr(2, object_t.length - 4); }
				
				var exist = false;
				
				$(tags_by_imp).each(function (index_ti, object_ti) {
						if (object_ti[0] == tag)
						{ 
							exist = true;
							tags_by_imp[index_ti] = [tags_by_imp[index_ti][0], tags_by_imp[index_ti][1] + 1];
						}
				});
				
				if (exist == false)
				{ tags_by_imp.push([tag, 1]); }
			});
		}
	});
		
	tags_by_imp = tags_by_imp.sort(function(a,b){return a[1] < b[1]});
	
	// Display Bug Fix 
	if (tags_by_imp.length >= 5) {
		if (tags_by_imp[4][0].slice(-1) == '"')
		tags_by_imp[4][0] = tags_by_imp[4][0].substr(0,tags_by_imp[4][0].length-2);
	}
	
	new Chartkick.PieChart("chart", tags_by_imp.slice(0,5));
}

function top_tags_click()
{
	$("#perf_capt").text("Top 5 Most Clicked Tags");
	$("#top").css("display", "none");
	$("#chart").css("display", "flex");
	
	var tags_by_click = []
	
	$(".ad-info-click").each (function (index, object_i) {
		var go = false;
		
		$("#ads_filter option:selected").each(function (index, object) {
			if ($(object).val() == $(object_i).val().split("¬")[0])
			{ 
				go = true;
			}
		});
		
		if (go == true)
		{
			$($(object_i).val().split("¬")[1].split(",")).each (function (index_t, object_t) {
				var tag;
				
				if (index_t != $(".ad-info-click").val().split("¬")[1].split(",").length - 1)
				{ tag = object_t.substr(2, object_t.length - 3); }
				else
				{ tag = object_t.substr(2, object_t.length - 4); }
				
				var exist = false;
				
				$(tags_by_click).each(function (index_ti, object_ti) {
						if (object_ti[0] == tag)
						{ 
							exist = true;
							tags_by_click[index_ti] = [tags_by_click[index_ti][0], tags_by_click[index_ti][1] + 1];
						}
				});
				
				if (exist == false)
				{ tags_by_click.push([tag, 1]); }
			});
		}
	});
	
	tags_by_click = tags_by_click.sort(function(a,b){return a[1] < b[1]});
	
	// Display Bug Fix 
	if (tags_by_click.length >= 5) {
		if (tags_by_click[4][0].slice(-1) == '"')
		tags_by_click[4][0] = tags_by_click[4][0].substr(0,tags_by_click[4][0].length-2);
	}
	
	new Chartkick.PieChart("chart", tags_by_click.slice(0,5));
}

function ctr()
{
	$("#perf_capt").text("CTR");
	$("#top").css("display", "inline");
	$("#chart").css("display", "none");
	$("#top").html("");
	$("#top").append("<ul class='top-list'></ul>");
	
	var imp = [];
	var click = [];
	
		$(".imp-date").each(function (index, object_d) {
				var go = false
				$("#ads_filter option:selected").each(function (index, object) {
					if ($(object).val() == $(object_d).val().split("¬")[1])
					{ 
						go = true;
					}
				});
				
				if (go == true)
				{ 
					var exist = false;
					
					$(imp).each (function (index_i, object_i) {
						if (object_i[0] == $(object_d).val().split("¬")[1])
						{
							exist = true;
							imp[index_i] = [imp[index_i][0], imp[index_i][1] + 1];
						}
					});
					
					if (exist == false)
					{ imp.push([$(object_d).val().split("¬")[1], 1]);}
				}
		});
	
	$(".click-date").each(function (index, object_d) {
			var go = false
			$("#ads_filter option:selected").each(function (index, object) {
				if ($(object).val() == $(object_d).val().split("¬")[1])
				{ 
					go = true;
				}
			});
			
			if (go == true)
			{ 
				var exist = false;
				
				$(click).each (function (index_c, object_c) {
					if (object_c[0] == $(object_d).val().split("¬")[1])
					{
						exist = true;
						click[index_c] = [click[index_c][0], click[index_c][1] + 1];
					}
				});
				
				if (exist == false)
				{ click.push([$(object_d).val().split("¬")[1], 1]);}
			}
	});
	
	imp = imp.sort(function(a,b){return a[1] < b[1]});
	click = click.sort(function(a,b){return a[1] < b[1]});
	
	var ctr = []
	
	$(imp).each (function (index, object) {
		$(click).each (function (index_c, object_c)
		{
			if (object[0] == object_c[0])
			{ ctr.push([object[0], (object_c[1] / object[1]) * 100]);}
		});
	});
	
	ctr = ctr.sort(function(a,b){return a[1] < b[1]});
	
	$(ctr).each (function (index,object) {
		$(".ad-info").each (function (index_i, object_i) {
			if ($(object_i).val().split("¬")[1] == object[0])
			{ ctr[index] = [$(object_i).val().split("¬")[0], ctr[index][1]]; }
		});
	});

	if (ctr.length == 0)
	{ $(".top-list").append("<li> No Data </li>"); }
	else
	{
		$(ctr).each (function (index, object) {
			$(".top-list").append("<li>" + object[0] + " - " + object[1].toString().substr(0, 6) + "%</li>");
		});
	}
}

function ctr_dynamics()
{
	$("#perf_capt").text("CTR Dynamics");
	$("#top").css("display", "none");
	$("#chart").css("display", "flex");
	
	var imp = [];
	var click = [];
	
	$(".imp-date").each(function (index, object_d) {
			var go = false
			$("#ads_filter option:selected").each(function (index, object) {
				if ($(object).val() == $(object_d).val().split("¬")[1])
				{ 
					go = true;
				}
			});
			
			if (go == true)
			{ 
				var exist = false;
				
				$(imp).each (function (index_i, object_i) {
					if (object_i[0] == $(object_d).val().split("¬")[1] && object_i[2] == $(object_d).val().split("¬")[0])
					{
						exist = true;
						imp[index_i] = [imp[index_i][0], imp[index_i][1] + 1, $(object_d).val().split("¬")[0]];
					}
				});
				
				if (exist == false)
				{ imp.push([$(object_d).val().split("¬")[1], 1, $(object_d).val().split("¬")[0]]);}
			}
	});
	
	$(".click-date").each(function (index, object_d) {
			var go = false
			$("#ads_filter option:selected").each(function (index, object) {
				if ($(object).val() == $(object_d).val().split("¬")[1])
				{ 
					go = true;
				}
			});
			
			if (go == true)
			{ 
				var exist = false;
				
				$(click).each (function (index_c, object_c) {
					if (object_c[0] == $(object_d).val().split("¬")[1] && object_c[2] == $(object_d).val().split("¬")[0])
					{
						exist = true;
						click[index_c] = [click[index_c][0], click[index_c][1] + 1, $(object_d).val().split("¬")[0]];
					}
				});
				
				if (exist == false)
				{ click.push([$(object_d).val().split("¬")[1], 1, $(object_d).val().split("¬")[0]]);}
			}
	});
	
	imp = imp.sort(function(a,b){return a[1] < b[1]});
	click = click.sort(function(a,b){return a[1] < b[1]});
	
	var ctr = []
	
	$(imp).each (function (index, object) {
		$(click).each (function (index_c, object_c)
		{
			if (object[0] == object_c[0] && object[2] == object_c[2])
			{ ctr.push([object[0], (object_c[1] / object[1]) * 100, object[2]]);}
		});
	});
	
	ctr = ctr.sort(function(a,b){return a[1] < b[1]});
	
	var date_vals = [];
	
	$(ctr).each (function (index,object) {
		$(".ad-info").each (function (index_i, object_i) {
			if ($(object_i).val().split("¬")[1] == object[0])
			{ 
			ctr[index] = [$(object_i).val().split("¬")[0], ctr[index][1], ctr[index][2]]; 
			}
		});
	});
	
	$(ctr).each (function (index_c, object_c) {
			var exist = false;
			
			$(date_vals).each (function (index, object) {
				if (object["name"] == object_c[0])
				{
					exist = true;
                    date_vals[index]["data"][ctr[index_c][2]] = ctr[index_c][1].toString().substr(0,6);
				}
			});
			
			if (exist == false)
			{ 
			  date_vals.push({"name": ctr[index_c][0], "data": {}}); 
			  date_vals[date_vals.length - 1]["data"][ctr[index_c][2]] = ctr[index_c][1].toString().substr(0,6);
			}
	});
	
	new Chartkick.LineChart("chart", date_vals);
	
}

function imp_dynamics()
{
	$("#perf_capt").text("Impressions Dynamics");
	dyn(".imp-date");
}

function click_dynamics()
{
	$("#perf_capt").text("Clicks Dynamics");
	dyn(".click-date");
}	

function ctr_ages() 
{
	$("#perf_capt").text("Top 5 Ages By CTR");
	$("#top").css("display", "inline");
	$("#chart").css("display", "none");
	$("#top").html("");
	$("#top").append("<ul class='top-list'></ul>");
	
	var imp = [];
	var click = [];
	
	$(".imp-date").each(function (index, object_d) {
			var go = false
			$("#ads_filter option:selected").each(function (index, object) {
				if ($(object).val() == $(object_d).val().split("¬")[1])
				{ 
					go = true;
				}
			});
			
			if (go == true)
			{ 
				var exist = false;
				
				$(imp).each (function (index_i, object_i) {
					if (object_i[0] == $(object_d).val().split("¬")[3])
					{
						exist = true;
						imp[index_i] = [imp[index_i][0], imp[index_i][1] + 1];
					}
				});
				
				if (exist == false)
				{ imp.push([$(object_d).val().split("¬")[3], 1]);}
			}
	});
	
	$(".click-date").each(function (index, object_d) {
			var go = false
			$("#ads_filter option:selected").each(function (index, object) {
				if ($(object).val() == $(object_d).val().split("¬")[1])
				{ 
					go = true;
				}
			});
			
			if (go == true)
			{ 
				var exist = false;
				
				$(click).each (function (index_c, object_c) {
					if (object_c[0] == $(object_d).val().split("¬")[3])
					{
						exist = true;
						click[index_c] = [click[index_c][0], click[index_c][1] + 1];
					}
				});
				
				if (exist == false)
				{ click.push([$(object_d).val().split("¬")[3], 1]);}
			}
	});
	
	imp = imp.sort(function(a,b){return a[1] < b[1]});
	click = click.sort(function(a,b){return a[1] < b[1]});
	
	var ctr = []
	
	$(imp).each (function (index, object) {
		$(click).each (function (index_c, object_c)
		{
			if (object[0] == object_c[0])
			{ ctr.push([object[0], (object_c[1] / object[1]) * 100]);}
		});
	});
	
	ctr = ctr.sort(function(a,b){return a[1] < b[1]});

		var max = 0;
		
		$(ctr).each (function (index, object) {
			if (object[0] != "Unknown")
			{
				$(".top-list").append("<li>" + (index+1) + ". " + object[0] + " Years Old - " + object[1].toString().substr(0, 6) + "%</li>");
				max = index + 1;
			}
		});
		
		if (max < 5)
		{
			for (i = max + 1; i < 6; i++)
			{
				$(".top-list").append("<li>" + i + ". N/A</li>");
			}
		}
		
}

function upd()
{
		if ($("#perf_selector").val() == "Impressions By Month") { imp_by_m(); }
		else if ($("#perf_selector").val() == "Impressions By Year") { imp_by_y(); }
		else if ($("#perf_selector").val() == "Clicks By Month") { clicks_by_m(); }
		else if ($("#perf_selector").val() == "Clicks By Year") { clicks_by_y(); }
		else if ($("#perf_selector").val() == "Top 5 Countries By Clicks") { top_countries_by_clicks(); }
		else if ($("#perf_selector").val() == "Top 5 Countries By Impressions") { top_countries_by_impressions(); }
		else if ($("#perf_selector").val() == "Top 5 Most Retrieved Ads") { top_ads_imp(); }
		else if ($("#perf_selector").val() == "Top 5 Most Clicked Ads") { top_ads_click(); }
		else if ($("#perf_selector").val() == "Top 5 Most Retrieved Tags") { top_tags_imp(); }
		else if ($("#perf_selector").val() == "Top 5 Most Clicked Tags") { top_tags_click(); }
		else if ($("#perf_selector").val() == "CTR") { ctr(); }
		else if ($("#perf_selector").val() == "CTR Dynamics") { ctr_dynamics(); }
		else if ($("#perf_selector").val() == "Impressions Dynamics") { imp_dynamics(); }
		else if ($("#perf_selector").val() == "Clicks Dynamics") { click_dynamics(); }
		else if ($("#perf_selector").val() == "Top 5 Ages By CTR") { ctr_ages(); }
}

function by_m(val) {
	$("#chart").css("display", "flex");
	$("#top").css("display", "none");
	var months = [0,0,0,0,0,0,0,0,0,0,0,0]
	
	$(val).each(function (index, object_d) {
		if ($(this).val().split("-")[0] == $("#cur_year").val())
		{
			var go = false
			$("#ads_filter option:selected").each(function (index, object) {
				if ($(object).val() == $(object_d).val().split("¬")[1])
				{ 
					go = true;
				}
			});
			
			if (go == true)
			{ months[parseInt($(this).val().split("-")[1]) - 1] = months[parseInt($(this).val().split("-")[1]) - 1] + 1; }
		}
	});
	
	var empty = true;
	
	for (i = 0; i < months.length; i++)
	{
		if (months[i] != 0) { empty = false; }
		switch (i) {
		case 0:
			months[i] = ["January", months[i]];
			break;
		case 1:
			months[i] = ["February", months[i]];
			break;
		case 2:
			months[i] = ["March", months[i]];
			break;
		case 3:
			months[i] = ["April", months[i]];
			break;
		case 4:
			months[i] = ["May", months[i]];
			break;
		case 5:
			months[i] = ["June", months[i]];
			break;
		case 6:
			months[i] = ["July", months[i]];
			break;
		case 7:
			months[i] = ["August", months[i]];
			break;
	    case 8:
			months[i] = ["September", months[i]];
			break;
		case 9:
			months[i] = ["October", months[i]];
			break;
		case 10:
			months[i] = ["November", months[i]];
			break;
		case 11:
			months[i] = ["December", months[i]];
			break;
		} 
	}
	
	if (empty == true) { months = []; }
	
	new Chartkick.PieChart("chart", months);
}

function by_y(val) 
{
	$("#chart").css("display", "flex");
	$("#top").css("display", "none");
	var years = [];
	
	$(val).each(function (index, object_d) {
		var go = false
		$("#ads_filter option:selected").each(function (index, object) {
			if ($(object).val() == $(object_d).val().split("¬")[1])
			{ 
				go = true;
			}
		});
			
		if (go == true)
		{
			for (i = 0; i < years.length; i++) {
				if (years[i][0] == $(this).val().split("-")[0]) { years[i][1] = years[i][1] + 1 }
			}
			
			var exist = false
			
			for (i = 0; i < years.length; i++) {
				if (years[i][0] == $(this).val().split("-")[0]) { exist = true }
			}
			
			if (exist == false) { years.push([$(this).val().split("-")[0], 1]); }
		}
	});

	new Chartkick.PieChart("chart", years);
}

function top_country(val)
{
	$("#top").css("display", "inline");
	$("#chart").css("display", "none");
	$("#top").html("");
	$("#top").append("<ul class='top-list'></ul>");
	
	var countries = [];
	
	$(val).each(function (index, object_d) {
		var go = false;
		$("#ads_filter option:selected").each(function (index, object) {
			if ($(object).val() == $(object_d).val().split("¬")[1])
			{ 
				go = true;
			}
		});
		
		if (go == true)
		{
			for (i = 0; i < countries.length; i++) {
				if (countries[i][0] == $(this).val().split("¬")[0]) { countries[i][1] = countries[i][1] + 1 }
			}
			
			var exist = false
			
			for (i = 0; i < countries.length; i++) {
				if (countries[i][0] == $(this).val().split("¬")[0]) { exist = true }
			}
			
			if (exist == false) { countries.push([$(this).val().split("¬")[0], 1]); }
		}
	});
	
	countries = countries.sort(function(a,b){return a[1] < b[1]})
	
	var num = 0
	
	$(countries).each(function (index, object) {
		if (object[0].split("¬")[0] != "N/A")
		{
			num++;
			$(".top-list").append("<li>" + num + ". " + object[0].split("¬")[0] + "</li>");
		}
	});
	
	if (num < 5)
	{
		for (i = 5 - (5 - num); i < 5; i++) 
		{
			$(".top-list").append("<li>" + (i+1) + ". N/A </li>");
		}
	}	
}

function top_ad(val)
{
	$("#top").css("display", "inline");
	$("#chart").css("display", "none");
	$("#top").html("");
	$("#top").append("<ul class='top-list'></ul>");
	
	var ad_by_imp = []
	
	$(val).each(function (index, object_d) {
		var go = false;
		
		$("#ads_filter option:selected").each(function (index, object) {
			if ($(object).val() == $(object_d).val().split("¬")[1])
			{ 
				go = true;
			}
		});
		
		
		if (go == true)
		{
			var exist = false;
			
			$(ad_by_imp).each(function (index, object) {
				if (object[0] == $(object_d).val().split("¬")[1]) 
				{ 
					exist = true; 
					ad_by_imp[index] = [ad_by_imp[index][0], ad_by_imp[index][1] + 1];
				}
			});
			
			if (exist == false)
			{ ad_by_imp.push([$(object_d).val().split("¬")[1], 1]); }
		}		
	});
	
	ad_by_imp = ad_by_imp.sort(function(a,b){return a[1] < b[1]});
	
	$(ad_by_imp).each(function (index, object) {
			$(".ad-info").each(function (index_i, object_i) {
				if ($(object_i).val().split("¬")[1] == object[0])
				{
					ad_by_imp[index] = $(object_i).val().split("¬")[0];
				}
			});
	});
	
	var max = 0;
	
	$(ad_by_imp).each(function (index, object) {
		$(".top-list").append("<li>" + (index+1) + ". " + ad_by_imp[index] + "</li>");
		max = index + 1;
	});
	
	if (max < 5)
	{
		for (i = max + 1; i < 6; i++)
		{
			$(".top-list").append("<li>" + i + ". N/A</li>");
		}
	}
}

function dyn(val)
{
	$("#top").css("display", "none");
	$("#chart").css("display", "flex");
	
	var results = [];

	$(val).each(function (index, object_d) {
			var go = false
			$("#ads_filter option:selected").each(function (index, object) {
				if ($(object).val() == $(object_d).val().split("¬")[1])
				{ 
					go = true;
				}
			});
			
			if (go == true)
			{ 
				var exist = false;
				
				$(results).each (function (index_r, object_r) {
					if (object_r[0] == $(object_d).val().split("¬")[1] && object_r[2] == $(object_d).val().split("¬")[0])
					{
						exist = true;
						results[index_r] = [results[index_r][0], results[index_r][1] + 1, $(object_d).val().split("¬")[0]];
					}
				});
				
				if (exist == false)
				{ results.push([$(object_d).val().split("¬")[1], 1, $(object_d).val().split("¬")[0]]);}
			}
	});
	
	results = results.sort(function(a,b){return a[1] < b[1]});
	
	var date_vals = [];
	
	$(results).each (function (index,object) {
		$(".ad-info").each (function (index_i, object_i) {
			if ($(object_i).val().split("¬")[1] == object[0])
			{ 
			results[index] = [$(object_i).val().split("¬")[0], results[index][1], results[index][2]]; 
			}
		});
	});
	
	$(results).each (function (index_r, object_r) {
			var exist = false;
			
			$(date_vals).each (function (index, object) {
				if (object["name"] == object_r[0])
				{
					exist = true;
                    date_vals[index]["data"][results[index_r][2]] = results[index_r][1].toString().substr(0,6);
				}
			});
			
			if (exist == false)
			{ 
			  date_vals.push({"name": results[index_r][0], "data": {}}); 
			  date_vals[date_vals.length - 1]["data"][results[index_r][2]] = results[index_r][1].toString().substr(0,6);
			}
	});
	
	new Chartkick.LineChart("chart", date_vals);	
}
