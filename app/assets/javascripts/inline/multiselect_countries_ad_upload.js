$(function(){
   $("#ad_countries").multiselect({
	    noneSelectedText: "Choose at least one country",
	    click: function(event, ui){
            if ($("#ad_countries").multiselect("getChecked").length == 0)
            { $("#submit-button").attr("disabled", "disabled"); }
            else
            { $("#submit-button").attr("disabled", false); }
            
            if (ui.checked == true)
            {
				if ($("#c_vals").val() != "") { $("#c_vals").val($("#c_vals").val() + "," + ui.value); }
				else { $("#c_vals").val(ui.value); }
			}
			
			else
			{
				values = $("#c_vals").val().split(ui.value + ",")
				if (values.length == 2)
				{ $("#c_vals").val(values[0] + values[1])}
				else if ($("#c_vals").val().indexOf(",") == -1)
				{ $("#c_vals").val(""); }
			}
        },        
        uncheckAll: function(event, ui){
            if ($("#ad_countries").multiselect("getChecked").length == 0)
            { $("#submit-button").attr("disabled", "disabled"); }
            else
            { $("#submit-button").attr("disabled", false); }
            $("#c_vals").val("");
		},
		
		checkAll: function(Even, ui){
			$("#submit-button").attr("disabled", false);
			$("#c_vals").val("Afghanistan,Albania,Algeria,Andorra,Angola,Antigua and Barbuda,Argentina,Armenia,Aruba,Australia,Austria,Azerbaijan,Bahamas,Bahrain,Bangladesh,Barbados,Belarus,Belgium,Belize,Benin,Bhutan,Bolivia,Bosnia and Herzegovina,Botswana,Brazil,Brunei,Bulgaria,Burkina Faso,Burma,Burundi,Cambodia,Cameroon,Canada,Cape Verde,Central African Republic,Chad,Chile,China,Colombia,Comoros,Costa Rica,Cote d'Ivoire,Croatia,Cuba,Curacao,Cyprus,Czech Republic,Democratic Republic of the Congo,Denmark,Djibouti,Dominica,Dominican Republic,East Timor,Ecuador,Egypt,El Salvador,Equatorial Guinea,Eritrea,Estonia,Ethiopia,Fiji,Finland,France,Gabon,Gambia,Georgia,Germany,Ghana,Greece,Grenada,Guatemala,Guinea,Guinea-Bissau,Guyana,Haiti,Holy See,Honduras,Hong Kong,Hungary,Iceland,India,Indonesia,Iran,Iraq,Ireland,Israel,Italy,Jamaica,Japan,Jordan,Kazakhstan,Kenya,Kiribati,North Korea,South Korea,Kosovo,Kuwait,Kyrgyzstan,Laos,Latvia,Lebanon,Lesotho,Liberia,Libya,Liechtenstein,Lithuania,Luxembourg,Macau,Macedonia,Madagascar,Malawi,Malaysia,Maldives,Mali,Malta,Marshall Islands,Mauritania,Mauritius,Mexico,Micronesia,Moldova,Monaco,Mongolia,Montenegro,Morocco,Mozambique,Namibia,Nauru,Nepal,Netherlands,Netherlands Antilles,New Zealand,Nicaragua,Niger,Nigeria,Norway,Oman,Pakistan,Palau,Palestinian Territories,Panama,Papua New Guinea,Paraguay,Peru,Philippines,Poland,Portugal,Qatar,Republic of the Congo,Romania,Russia,Rwanda,Saint Kitts and Nevis,Saint Lucia,Saint Vincent and the Grenadines,Samoa,San Marino,Sao Tome and Principe,Saudi Arabia,Senegal,Serbia,Seychelles,Sierra Leone,Singapore,Sint Maarten,Slovakia,Slovenia,Solomon Islands,Somalia,South Africa,South Sudan,Spain,Sri Lanka,Sudan,Suriname,Swaziland,Sweden,Switzerland,Syria,Taiwan,Tajikistan,Tanzania,Thailand,Timor-Leste,Togo,Tonga,Trinidad and Tobago,Tunisia,Turkey,Turkmenistan,Tuvalu,Uganda,Ukraine,United Arab Emirates,United Kingdom,United States,Uruguay,Uzbekistan,Vanuatu,Venezuela,Vietnam,Yemen,Zambia,Zimbabwe");
		}
	   }); 
	   
  if ($("#init_c_vals").val() == null)	    
  $("#ad_countries").multiselect("checkAll");
  
  else
  {
	  $("#ui-multiselect-ad_countries-option-0").click();
	  $.each($("#init_c_vals").val().split(" "), function (index, obj) {
			$("#ui-multiselect-ad_countries-option-"+obj).click();
	  });  
  }
  
  $("#ad_countries").attr('disabled', false);
});

$(document).ready(function() {
	$("#ad_countries").attr('disabled', 'disabled');
	$('.ui-multiselect').css('width', '65%');
	width = $('.ui-multiselect').css('width');
	$('.ui-multiselect-menu.ui-widget.ui-widget-content.ui-corner-all').attr('style', 'width: ' + width + ';');
});
