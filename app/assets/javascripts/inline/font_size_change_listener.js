$(document).ready(function () {

size = parseInt($("#font-size-val").text());
size_set(size);

$(function() {
	$("#font-increase-button").click(function() { 
		size++;
		size_set(size);
		try
		{
		$.get('/inc_font');
		}
		catch (e) {}
	});

	$("#font-decrease-button").click(function() { 
		size--;
		size_set(size);
		try
		{
		$.get('/dec_font');
		}
		catch (e) {}
	});
	});
});

function size_set(size) { switch (size) {
    case 0:
		$("body").attr("class", "");
		$("h1").attr("class", "");
		$(".btn").removeClass("larger");
		$(".btn").removeClass("largest");
		break;
    case 1:
        $("body").attr("class", "larger");
		$("h1").attr("class", "larger");
		$(".btn").addClass("larger");
		$(".btn").removeClass("largest");
		break;
    case 2:
        $("body").attr("class", "largest");
		$("h1").attr("class", "largest");
		$(".btn").removeClass("larger");
		$(".btn").addClass("largest");
        break;
};}
