$(document).ready(function () {
	$(".btn-show-ad-countries").click(function () {
		if ($(".btn-show-ad-countries").text() == 'Show Countries Served')
		{
			$(".btn-show-ad-countries").text('Hide Countries Served');
			$(".countries-served").css("display", "inline-block");
		}
		else
		{
			$(".btn-show-ad-countries").text('Show Countries Served');
			$(".countries-served").css("display", "none");
		}
	});
});
