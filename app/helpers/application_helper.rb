module ApplicationHelper

	def countries_list
		@countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Antigua and Barbuda",
		"Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh",
		"Barbados","Belarus","Belgium","Belize","Benin","Bhutan","Bolivia","Bosnia and Herzegovina",
		"Botswana","Brazil","Brunei","Bulgaria","Burkina Faso","Burma","Burundi",
		"Cambodia","Cameroon","Canada","Cape Verde","Central African Republic","Chad","Chile","China",
		"Colombia","Comoros","Costa Rica","Cote d'Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic",
		"Democratic Republic of the Congo","Denmark","Djibouti","Dominica","Dominican Republic","East Timor",
		"Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Fiji","Finland",
		"France","Gabon","Gambia","Georgia","Germany","Ghana","Greece","Grenada","Guatemala","Guinea",
		"Guinea-Bissau","Guyana","Haiti","Holy See","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia",
		"Iran","Iraq","Ireland","Israel","Italy","Jamaica","Japan","Jordan","Kazakhstan","Kenya","Kiribati",
		"North Korea","South Korea","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia",
		"Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia",
		"Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova",
		"Monaco","Mongolia","Montenegro","Morocco","Mozambique","Namibia","Nauru","Nepal","Netherlands",
		"Netherlands Antilles","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman",
		"Pakistan","Palau","Palestinian Territories","Panama","Papua New Guinea","Paraguay","Peru","Philippines",
		"Poland","Portugal","Qatar","Republic of the Congo","Romania","Russia","Rwanda","Saint Kitts and Nevis",
		"Saint Lucia","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia",
		"Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Sint Maarten","Slovakia","Slovenia",
		"Solomon Islands","Somalia","South Africa","South Sudan","Spain","Sri Lanka","Sudan",
		"Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor-Leste",
		"Togo","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Tuvalu","Uganda","Ukraine","United Arab Emirates",
		"United Kingdom","United States","Uruguay","Uzbekistan","Vanuatu","Venezuela","Vietnam","Yemen","Zambia","Zimbabwe"]
	end

	# Ads search
	def search_results(search_query)
		search_query = search_query.downcase
		@ads = []

		Ad.all.each do |a|
			if a.desc.downcase.include? search_query or a.link.include? search_query
				@ads.push(a)
			end
			
			a.tags.split(',').each do |t|
				if t.include? search_query
					@ads.push(a)
				end
			end
			
			a.countries.each do |c|
				if countries_list[c].downcase.include? search_query
					@ads.push(a)
				end
			end
		end
		
		if @ads.length == 0
			Ad.all.each do |a|
				a.tags.split(',').each do |t|
					search_query.split(',').each do |st|
						if st == t
							@ads.push(a)
						end
					end
				end
			end
		end
		
		@ads = @ads.uniq
		
		if current_user.utype == 0
			@ads = @ads.delete_if { |a| a.publisher != current_user.id}
		end
	end

	def ad_blocked_in_products(ad_id)
		@block_list = []
		
		begin
			Ad.find(ad_id).blocked_tags.each do |bt|
				current_user.products.each do |up|
					if bt[0] == up.id and bt[1] == "x"
						@block_list.push(up.id)
					end
				end
			end
		rescue # If no blocked tags
		end
	end

	# Checking if the same ad was clicked from the same IP more than three times in two minutes
	def check_if_scam(ad_id, click)
		@ip_log = Ip.where(:ad => ad_id).where(:click => click).where(:ip => request.remote_ip).first
		if @ip_log == nil
			@ip_log = Ip.new(ad: ad_id, hits: 0, ip: request.remote_ip, click: click)
			@ip_log.save
		end
		
		if click == false
			@time = 2.minutes
		else
			@time = 5.minutes
		end
		
		if @ip_log.hits >= 3 and @ip_log.updated_at.to_i - @ip_log.created_at.to_i <= @time
			@ip_log.update_attribute(:hits, @ip_log.hits + 1)
			true # Suspicious Behaviour
		elsif @ip_log.hits >= 3 and @ip_log.updated_at.to_i - @ip_log.created_at.to_i > @time
			@ip_log.update_attribute(:created_at, Time.now)
			@ip_log.update_attribute(:hits, 0)
			false
		elsif @ip_log.hits < 3
			@ip_log.update_attribute(:hits, @ip_log.hits + 1)
			false
		end 
	end
	  
	# Prevent ads serving to the restricted territories   
	def restrict_country(num_vals)
		@geoip ||= GeoIP.new("./db/GeoIP.dat")    
		remote_ip = request.remote_ip 
		usr_location = @geoip.country(request.remote_ip).country_name
		
		@restrict = true
		
		if usr_location == "N/A"
			@restrict = false
		else
			num_vals.each do |c_val|
				c_name = countries_list[c_val]
				if c_name == usr_location
					@restrict = false # Allow
				end
			end
		end
		
		@restrict
	end

end
