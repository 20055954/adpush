module SessionsHelper

      # Remembers a user in a persistent session.
      def remember(user)
        user.remember
        cookies.permanent.signed[:user_id] = user.id
        cookies.permanent[:remember_token] = user.remember_token
      end

      def current_user
        if (user_id = cookies.signed[:user_id])
           user = User.find_by(id: user_id)
           if user && 
                user.authenticated?(cookies[:remember_token])
            @current_user = user
          end
        end
      end

      def logged_in?
		   !current_user.nil?
      end
      
      def banned?
			if current_user.banned == true
				true
			else
				false
			end
	  end
      
      def restrict_access
		if logged_in? == false
			redirect_to root_path
		end
		if banned?
			flash[:error]="You Are Banned"
			redirect_to root_path
		end
	  end
	  
	  def restrict_access_ignore_ban
		if logged_in? == false
			redirect_to root_path
		end
	  end
	  
	  def restrict_non_verified_access
		if current_user != nil
			if current_user.email_verification_key != nil 
				redirect_to current_user
			end
		else
			redirect_to root_path
		end
	  end
	  
	  def restrict_access_banned
		if banned?
			if current_user.banned < Date.today - 7.days
				User.find(current_user.id).update_attribute(:banned, nil)
				flash[:success] = "The Ban Period Is Over!"
				redirect_to current_user
			else
				flash[:error] = "You Are Temporarily Banned"
				redirect_to root_path
			end
	    end
	  end
	  
      # Returns true if the given user is the current user.
      def current_user?(user)
        user == current_user
      end

       # Forgets a persistent session.
      def forget(user)
          user.forget
          cookies.delete(:user_id)
          cookies.delete(:remember_token)
      end

      # Logs out the current user.
      def log_out
          forget(current_user)
          @current_user = nil
          redirect_to root_path
      end

end
