class PaymentCard < ActiveRecord::Base
	belongs_to :user
	
	before_save {self.card_num = self.card_num.to_i.to_s }
	
	NUM_REGEX = /\A[0-9]{16}\Z/
	      
	validates :card_num, format: { with: NUM_REGEX }
	
	validate :unique_card_num
	validate :exp_date
      
    def unique_card_num
      	PaymentCard.all.each do |pc|
			if pc.card_num == self.card_num
				errors.add(:card_num, 'must be unique')
				break
			end
		end
	end
	
	def exp_date
		if self.expiry_date_yy == Date.today.year and self.expiry_date_mm <= Date.today.month
			errors.add(:expiry_date_mm, 'expired')
		end
	end
end
