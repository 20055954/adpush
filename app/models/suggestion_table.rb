class SuggestionTable
  include Mongoid::Document

  field :country, type: String
  field :age, type: Integer
  field :gender, type: String 
  field :fav_ads, type: Array 
  field :decade, type: Boolean
  field :quadrant, type: Boolean
end
