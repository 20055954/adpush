class Tag < ActiveRecord::Base
	has_many :blocked_tags, :foreign_key => 'tag_id'
	before_save { self.name = self.name.gsub!("¬", "") }
end
