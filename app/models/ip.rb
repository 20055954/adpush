class Ip
  include Mongoid::Document 
  include Mongoid::Timestamps

  field :ad, type: String
  field :hits, type: Integer
  field :ip, type: String
  field :click, type: Boolean
end
