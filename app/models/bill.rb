class Bill < ActiveRecord::Base
	belongs_to :user
	
	def created_at_label
		created_at.strftime("%Y-%m")
	end
	
end
