class Product < ActiveRecord::Base
	belongs_to :user
	has_many :blocked_tags, :foreign_key => 'product_id'
	
	VALID_FORMAT = /\A[A-Za-z0-9 _-]+¬\z/i
	
	before_save { self.title = self.title.downcase.capitalize.strip.gsub!("¬","") }
	validates :title, presence: true, length: { in: 2..120 }, format: { with: VALID_FORMAT, :message =>"is invalid (only alphanumeric characters, space, underscore, and dash are accepted)" }
end
