class User < ActiveRecord::Base

	attr_accessor :remember_token

	validates :login, presence: true, length: { in: 3..120}, format: { with: /\A[a-zA-Z0-9]+\Z/, message: "must contain no white spaces and special characters"}
	validates :name, presence: true, length: { in: 4..120}
	validates_length_of :name, minimum: 2, too_short: 'must be provided', tokenizer: ->(str) { str.scan(/\w+/) }
	validates :company, presence: true, length: { in: 2..120}
	validates :dob, presence: true, date: {after: Proc.new {Time.now - 100.years}, message: "must enter a valid date of birth"}
	validates :dob, date: {before: Proc.new {Date.today - 18.years}, message: "have to be at least 18 years old to use the service"}
	validates :state, presence: true
	validates :city, presence: true, length: {in: 2..57}
	validates :zip, presence: true
	validates :address, presence: true, length: {in: 10..200}

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, 
					format: { with: VALID_EMAIL_REGEX },
					uniqueness: { case_sensitive: false }
	validates :password, presence: true, length: { minimum: 6 }, :on => :create
	validates :password_confirmation, presence: true, :on => :create
	validates :password, presence: true, length: { minimum: 6 }, :on => :update, :allow_blank => true
	validates :password_confirmation, presence: true, :on => :update, :allow_blank => true

	has_secure_password

	validate :unique_company

	has_many :bank_infos, :foreign_key => 'user_id'
	has_many :bills, :foreign_key => 'user_id'
	has_many :payment_cards, :foreign_key => 'user_id'
	has_many :products, :foreign_key => 'user_id'
	has_many :feedbacks, :foreign_key => 'user_id'

	def unique_company
	company_c = self.company.strip.split.map(&:capitalize).join(' ')
	if self.company_was != company_c
		User.all.each do |u|
			if company_c == u.company and self.utype == u.utype
				errors.add(:company, 'must be unique')
				break
			end
		end
	end
	end

	# Returns the hash digest of a string.
	def User.digest(string)
	cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
												  BCrypt::Engine.cost
	BCrypt::Password.create(string, cost: cost)
	end

	# Returns a random token.
	def User.new_token
	SecureRandom.urlsafe_base64
	end

	# Remembers a user in the database for use in persistent sessions.
	def remember
	self.remember_token = User.new_token
	update_attribute(:remember_digest, User.digest(remember_token))
	end

	def authenticated?(remember_token)
	begin
		BCrypt::Password.new(remember_digest).is_password?(remember_token)
	rescue
		false
	end
	end

	def forget
	  update_attribute(:remember_digest, nil)
	end

end
