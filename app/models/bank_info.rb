class BankInfo < ActiveRecord::Base
	belongs_to :user
	has_many :invoices, :foreign_key => 'bank_acc_id'

	enc = "R37u9sFmENuff9KvyR4CYTg6"
	attr_encrypted :account_number, :key => enc, :mode => :per_attribute_iv_and_salt
	attr_encrypted :code, :key => enc, :mode => :per_attribute_iv_and_salt


	VALID_ACC_NUM_REGEX = /\A[a-zA-z][a-zA-z]+/
	BIC_REGEX = /\A[a-zA-z][a-zA-Z0-9]+\Z/

	validates :name, presence: true, length: { in: 3..120 }
	validates :account_number, presence: true, length: { in: 5..34 }, format: { with: VALID_ACC_NUM_REGEX }
	validates :code, presence: true, length: { in: 8..11 }, format: { with: BIC_REGEX}
	validates :state, presence: true
	validates :city, presence: true, length: {in: 2..57}
	validates :zip, presence: true
	validates :address, presence: true, length: {in: 10..200}

	validate :unique_iban

	def unique_iban
		BankInfo.all.each do |bi|
			if bi.account_number == self.account_number
				errors.add(:account_number, 'must be unique')
				break
			end
		end
	end
end
