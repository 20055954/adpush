class Ad
  include Mongoid::Document
  include Mongoid::Paperclip

  field :desc, type: String
  field :link, type: String
  has_mongoid_attached_file :content
  field :animation, type: Boolean, default: false
  field :publisher, type: Integer
  field :tags, type: Array
  field :min_age, type: Integer
  field :max_age, type: Integer
  field :countries, type: Array # Allowed countries
  field :active, type: Boolean, default: true
  field :clicks, type: Array
  field :impressions, type: Array
  field :blocked_tags, type: Array
  field :attachment_fingerprint, type: String 
  
  before_save { self.desc = self.desc.strip }
  before_save { self.link = self.link.downcase.strip }
        
  validate :valid_link
  validate :unique_link
  validate :unique_image
  
  validates_attachment_content_type :content, :content_type => ["image/png", "image/tiff", "image/gif"], :message => "is invalid (accepted formats: PNG, TIFF, GIF)"
  validates :desc, presence: true, length: { in: 3..120 }
  validates :link, presence: true
  validates :tags, presence: true
  validate :empty_tags
  validate :tags_num
  validates_attachment_size :content, :in => 0.megabytes..2.megabytes, :message => "must be less than 2 MB"
  
  def unique_link
	Ad.all.each do |a|
		if a.link == self.link and a.link != self.link_was
			errors.add(:link, 'must be unique')
			break
		end
	end
  end
  
  def tags_num
	if self.tags.count > 10
		errors.add(:tags, 'replace') # Replaced in Error View
	end
  end
  
  def empty_tags
  	all_blank = true
	
	self.tags.each do |t|
		if !t.blank?
			all_blank = false
		end
	end
	
	if all_blank
		errors.add(:tags, "can't be empty")
	end
  end
  
  def unique_image
		Ad.all.each do |a|
			if a.content_fingerprint == self.content_fingerprint and a.id != self.id and a.content_fingerprint != nil
				errors.add(:content, "must be unique")
				break
			end
		end
  end
  
  def valid_link
	if self.link[0..6] != "http://" and self.link[0..7] != "https://" and !self.link.empty?
		self.link = "http://" + self.link
	end
	
	begin
		!!URI.parse(self.link)
		
		if self.link.strip == "http://" or self.link.strip == "https://"
			errors.add(:link, 'is not valid')
		elsif self.link.split(".").size == 1
			errors.add(:link, 'is not valid')
		end
	
	rescue URI::InvalidURIError
		errors.add(:link, 'is not valid')
		false
	end
  end
  
end
