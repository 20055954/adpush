class Feedback < ActiveRecord::Base
	belongs_to :user
	
	validates :text, presence: true, length: { in: 10..600}
end
