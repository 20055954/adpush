namespace :events do
  desc "Suggestion tables creation"
  task :create_suggestion_table => :environment do
	
	@countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Antigua and Barbuda",
    "Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh",
    "Barbados","Belarus","Belgium","Belize","Benin","Bhutan","Bolivia","Bosnia and Herzegovina",
    "Botswana","Brazil","Brunei","Bulgaria","Burkina Faso","Burma","Burundi",
    "Cambodia","Cameroon","Canada","Cape Verde","Central African Republic","Chad","Chile","China",
    "Colombia","Comoros","Costa Rica","Cote d'Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic",
    "Democratic Republic of the Congo","Denmark","Djibouti","Dominica","Dominican Republic","East Timor",
    "Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Fiji","Finland",
    "France","Gabon","Gambia","Georgia","Germany","Ghana","Greece","Grenada","Guatemala","Guinea",
    "Guinea-Bissau","Guyana","Haiti","Holy See","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia",
    "Iran","Iraq","Ireland","Israel","Italy","Jamaica","Japan","Jordan","Kazakhstan","Kenya","Kiribati",
    "North Korea","South Korea","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia",
    "Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia",
    "Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova",
    "Monaco","Mongolia","Montenegro","Morocco","Mozambique","Namibia","Nauru","Nepal","Netherlands",
    "Netherlands Antilles","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman",
	"Pakistan","Palau","Palestinian Territories","Panama","Papua New Guinea","Paraguay","Peru","Philippines",
    "Poland","Portugal","Qatar","Republic of the Congo","Romania","Russia","Rwanda","Saint Kitts and Nevis",
    "Saint Lucia","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia",
    "Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Sint Maarten","Slovakia","Slovenia",
    "Solomon Islands","Somalia","South Africa","South Sudan","Spain","Sri Lanka","Sudan",
    "Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor-Leste",
    "Togo","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Tuvalu","Uganda","Ukraine","United Arab Emirates",
    "United Kingdom","United States","Uruguay","Uzbekistan","Vanuatu","Venezuela","Vietnam","Yemen","Zambia","Zimbabwe"]
    
	@geoip ||= GeoIP.new("./db/GeoIP.dat") 	
	@all_ads = []
	@tables_for_save = []	
	
	Ad.all.each do |ad|
		@all_ads.push ad
	end
	
	@countries.push "N/A"
	@gender = ['M','F', nil]
			
	@countries.each do |c|
		@gender.each do |g|
			(0..100).each do |a|
				fav_ads(c,g,a)
			end
		end
	end		
	
	@tables_for_save = @tables_for_save.delete_if{|tfs| tfs.fav_ads == nil}
	
	@tags = []
	
	@tables_for_save.each do |ts|
	    if ts.fav_ads != nil
			ts.fav_ads.each do |fa|
				@all_ads.each do |ad|
					if ad.id.to_s == fa[0]
						ad.tags.each do |t|
							if ad.impressions != nil and ad.clicks != nil
								@tags.push [ad.id.to_s, t, ad.clicks.length, ad.impressions.length]
							elsif ad.impressions == nil and ad.clicks != nil
								@tags.push [ad.id.to_s, t, ad.clicks.length, 0]
							elsif ad.impressions != nil and ad.clicks == nil
								@tags.push [ad.id.to_s, t, 0, ad.impressions.length]
							else
								@tags.push [ad.id.to_s, 0, 0, 0]
							end
						end
					end
				end
			end
		end
	end
	
	@tables_for_save.each do |tfs|
		if tfs.fav_ads != nil
		   ads_to_check = []
		   tfs.fav_ads.each do |fa|
			   ads_to_check.push [fa[0],fa[1],fa[2]]
		   end
		   until ads_to_check.length == 0 do
			  tfs.fav_ads.each_with_index do |fa, index|
				if ads_to_check[ads_to_check.length-1][1] == fa[1] and ads_to_check[ads_to_check.length-1][2] == fa[2]
					index_1 = nil
					index_2 = nil
					pos_2 = nil
					
					tfs.fav_ads.each_with_index do |fac, i|
						if ads_to_check[ads_to_check.length-1][0] == fac[0]
						   index_1 = i
						elsif fa[0] == fac[0]
							index_2 = i
							pos_2 = fac 
						end
					end
					
					if index != nil and index_2 != nil and pos_2 != nil
						## Now Actually Do Tags Check
						tags_1 = []
						tags_2 = []
						
						@tags.each do |t|
							if ads_to_check[ads_to_check.length-1][0] == t[0]
								tags_1.push [t[1], t[2], t[3]] # Tag Name, Tag Clicks, Tag Impressions
							elsif pos_2[0] == t[0]
								tags_2.push [t[1], t[2], t[3]]
						end
						
						tags_1_sum = [0,0] #Clicks, Impressions
						tags_2_sum = [0,0]
						
						tags_1.each do |t|
							if t[1] != nil
								tags_1_sum[0] = tags_1_sum[0] + t[1]
							end
							if t[2] != nil
								tags_1_sum[1] = tags_1_sum[1] + t[2]
							end
						end
						
						tags_2.each do |t|
							if t[1] != nil
								tags_2_sum[0] = tags_2_sum[0] + t[1]
							end
							if t[2] != nil
								tags_2_sum[1] = tags_2_sum[1] + t[2]
							end
						end						
						
					    if tags_1_sum != tags_2_sum
							if tags_2_sum[0] > tags_1_sum[0]
								tfs.fav_ads[index_1],tfs.fav_ads[index_2] = tfs.fav_ads[index_2],tfs.fav_ads[index_1]
							elsif tags_2_sum[0] == tags_1_sum[0]
								if tags_2_sum[1] > tags_1_sum[1]
									tfs.fav_ads[index_1],tfs.fav_ads[index_2] = tfs.fav_ads[index_2],tfs.fav_ads[index_1]
								end
							end
						end
						
					    end
					end
				end
			  end
			  ads_to_check.pop
		end
	end
	end

	@countries.each do |c|
	    decades_and_quadrants(c)	
	end
	
	# Do Top-10 Ads Only
	@tables_for_save.each do |tfs|
			tfs.fav_ads = tfs.fav_ads[0..9]	
	end

	@tables_for_save = @tables_for_save.delete_if{|tfs| tfs.fav_ads.length == 0}	
	
	SuggestionTable.destroy_all
	@tables_for_save.each do |s|
		s.save
	end    
    
  end
end

  def decades_and_quadrants(country)
		dec_val_m = [[0, "M", country, []],[10, "M", country, []],[20, "M", country, []],[30, "M", country, []],[40, "M", country, []],[50, "M", country, []],[60, "M", country, []],[70, "M", country, []],[80, "M", country, []],[90, "M", country, []]]
		dec_val_f = [[0, "F", country, []],[10, "F", country, []],[20, "F", country, []],[30, "F", country, []],[40, "F", country, []],[50, "F", country, []],[60, "F", country, []],[70, "F", country, []],[80, "F", country, []],[90, "F", country, []]]
		dec_val_nil = [[0, nil, country, []],[10, nil, country, []],[20, nil, country, []],[30, nil, country, []],[40, nil, country, []],[50, nil, country, []],[60, nil, country, []],[70, nil, country, []],[80, nil, country, []],[90, nil, country, []]]
		
		q_val_m = [[0, "M", country, []],[25, "M", country, []],[50, "M", country, []],[75, "M", country, []]]
		q_val_f = [[0, "F", country, []],[25, "F", country, []],[50, "F", country, []],[75, "F", country, []]]
		q_val_nil = [[0, nil, country, []],[25, nil, country, []],[50, nil, country, []],[75, nil, country, []]]
		
		(0..9).each do |i|
			@tables_for_save.each do |tfs|
				if tfs.fav_ads != nil
					if tfs.gender == "M" and (dec_val_m[i][0]..dec_val_m[i][0]+10).include?(tfs.age.to_i) and tfs.country == country
						tfs.fav_ads.each do |in_v|
							   dec_val_m[i][3].push in_v
						end
						
						sum_vals = []
						
						dec_val_m[i][3].each do |s|
							sum_vals.push s[0] unless sum_vals.include?(s[0])
						end
						
						sum_vals.each_with_index do |sv, index_sv|
							sum_vals[index_sv] = [sum_vals[index_sv], 0, 0]
						end
						
						sum_vals.each_with_index do |sv, sv_i|
							dec_val_m[i][3].each do |dc|
								if dc[0] == sum_vals[sv_i][0]
										sum_vals[sv_i][1] = sum_vals[sv_i][1] + dc[1] 
										sum_vals[sv_i][2] = sum_vals[sv_i][2] + dc[2]								
								end					
							end
						end
						
						if sum_vals.length != 0
							dec_val_m[i][3] = sum_vals.uniq
						end

					elsif tfs.gender == "F" and (dec_val_f[i][0]..dec_val_f[i][0]+10).include?(tfs.age.to_i) and tfs.country == country
					
						tfs.fav_ads.each do |in_v|
							   dec_val_f[i][3].push in_v
						end
						
						sum_vals = []
						
						dec_val_f[i][3].each do |s|
							sum_vals.push s[0] unless sum_vals.include?(s[0])
						end
						
						sum_vals.each_with_index do |sv, index_sv|
							sum_vals[index_sv] = [sum_vals[index_sv], 0, 0]
						end
						
						sum_vals.each_with_index do |sv, sv_i|
							dec_val_f[i][3].each do |dc|
								if dc[0] == sum_vals[sv_i][0]
										sum_vals[sv_i][1] = sum_vals[sv_i][1] + dc[1] 
										sum_vals[sv_i][2] = sum_vals[sv_i][2] + dc[2]								
								end					
							end
						end
						
						if sum_vals.length != 0
							dec_val_f[i][3] = sum_vals.uniq
						end
					
					elsif tfs.gender == nil and (dec_val_nil[i][0]..dec_val_nil[i][0]+10).include?(tfs.age.to_i) and tfs.country == country
					
						tfs.fav_ads.each do |in_v|
							   dec_val_nil[i][3].push in_v
						end
						
						sum_vals = []
						
						dec_val_nil[i][3].each do |s|
							sum_vals.push [s[0],0,0] unless sum_vals.include?(s[0])
						end
						
						sum_vals.each_with_index do |sv, sv_i|
							dec_val_nil[i][3].each do |dc|
								if dc[0] == sum_vals[sv_i][0]
										sum_vals[sv_i][1] = sum_vals[sv_i][1] + dc[1] 
										sum_vals[sv_i][2] = sum_vals[sv_i][2] + dc[2]								
								end					
							end
						end
						
						if sum_vals.length != 0
							dec_val_nil[i][3] = sum_vals.uniq
						end
						
					elsif i < 4 and tfs.gender == "M" and (q_val_m[i][0]..q_val_m[i][0]+25).include?(tfs.age.to_i) and tfs.country == country
					
						tfs.fav_ads.each do |in_v|
							   q_val_m[i][3].push in_v
						end
						
						sum_vals = []
						
						q_val_m[i][3].each do |s|
							sum_vals.push [s[0],0,0] unless sum_vals.include?(s[0])
						end
						
						sum_vals.each_with_index do |sv, sv_i|
							q_val_m[i][3].each do |dc|
								if dc[0] == sum_vals[sv_i][0]
										sum_vals[sv_i][1] = sum_vals[sv_i][1] + dc[1] 
										sum_vals[sv_i][2] = sum_vals[sv_i][2] + dc[2]								
								end					
							end
						end
						
						if sum_vals.length != 0
							q_val_m[i][3] = sum_vals.uniq
						end
												
					elsif i < 4 and tfs.gender == "F" and (q_val_f[i][0]..q_val_f[i][0]+25).include?(tfs.age.to_i) and tfs.country == country
					
						tfs.fav_ads.each do |in_v|
							   q_val_f[i][3].push in_v
						end
						
						sum_vals = []
						
						q_val_f[i][3].each do |s|
							sum_vals.push [s[0],0,0] unless sum_vals.include?(s[0])
						end
						
						sum_vals.each_with_index do |sv, sv_i|
							q_val_f[i][3].each do |dc|
								if dc[0] == sum_vals[sv_i][0]
										sum_vals[sv_i][1] = sum_vals[sv_i][1] + dc[1] 
										sum_vals[sv_i][2] = sum_vals[sv_i][2] + dc[2]								
								end					
							end
						end
						
						if sum_vals.length != 0
							q_val_f[i][3] = sum_vals.uniq
						end					
						
					elsif i < 4 and tfs.gender == "M" and (q_val_nil[i][0]..q_val_nil[i][0]+25).include?(tfs.age.to_i) and tfs.country == country
					
						tfs.fav_ads.each do |in_v|
							   q_val_nil[i][3].push in_v
						end
						
						sum_vals = []
						
						q_val_nil[i][3].each do |s|
							sum_vals.push [s[0],0,0] unless sum_vals.include?(s[0])
						end
						
						sum_vals.each_with_index do |sv, sv_i|
							q_val_nil[i][3].each do |dc|
								if dc[0] == sum_vals[sv_i][0]
										sum_vals[sv_i][1] = sum_vals[sv_i][1] + dc[1] 
										sum_vals[sv_i][2] = sum_vals[sv_i][2] + dc[2]								
								end					
							end
						end
						
						if sum_vals.length != 0
							q_val_nil[i][3] = sum_vals.uniq
						end	
						
					end
				end
			end
		end
		
		dec_val_m.each do |dv|
			@tables_for_save.push SuggestionTable.new(country: country, age: dv[0], gender: "M", fav_ads: dv[3].sort{|a,b| a[1] <=> b[1]}.sort{|a,b| a[2] <=> b[2]}.reverse!, quadrant: false, decade: true)		
		end
		dec_val_f.each do |dv|
			@tables_for_save.push SuggestionTable.new(country: country, age: dv[0], gender: "F", fav_ads: dv[3].sort{|a,b| a[1] <=> b[1]}.sort{|a,b| a[2] <=> b[2]}.reverse!, quadrant: false, decade: true)		
		end
		dec_val_nil.each do |dv|
			@tables_for_save.push SuggestionTable.new(country: country, age: dv[0], gender: nil, fav_ads: dv[3].sort{|a,b| a[1] <=> b[1]}.sort{|a,b| a[2] <=> b[2]}.reverse!, quadrant: false, decade: true)		
		end		
		q_val_m.each do |qv|
			@tables_for_save.push SuggestionTable.new(country: country, age: qv[0], gender: "M", fav_ads: qv[3].sort{|a,b| a[1] <=> b[1]}.sort{|a,b| a[2] <=> b[2]}.reverse!, quadrant: true, decade: false)		
		end
		q_val_f.each do |qv|
			@tables_for_save.push SuggestionTable.new(country: country, age: qv[0], gender: "F", fav_ads: qv[3].sort{|a,b| a[1] <=> b[1]}.sort{|a,b| a[2] <=> b[2]}.reverse!, quadrant: true, decade: false)		
		end
		q_val_nil.each do |qv|
			@tables_for_save.push SuggestionTable.new(country: country, age: qv[0], gender: nil, fav_ads: qv[3].sort{|a,b| a[1] <=> b[1]}.sort{|a,b| a[2] <=> b[2]}.reverse!, quadrant: true, decade: false)		
		end		
    end
        
    def fav_ads(c,g,a)
			    @st = SuggestionTable.new(country: c, gender: g, age: a, decade: false, quadrant: false)
			    @clicks = []
			    @impressions = []
			    
			    @all_ads.each do |ad|
					if ad.impressions != nil
						ad.impressions.each do |i|
							if i[2] == c and i[4].to_i == a and i[5] == g
							    if @impressions.length == 0
									@impressions = [[ad.id.to_s, 1]]
							    else
							        exist = false
									@impressions.each do |fav_ad_i|
										if fav_ad_i[0] == ad.id.to_s
										   fav_ad_i[1] = fav_ad_i[1] + 1
										   exist = true
										   break
										end
									end
									if !exist
										@impressions.push [ad.id.to_s, 1]
									end
								end
							end
						end
					end
					if ad.clicks != nil
						ad.clicks.each do |i|
							if i[2] == c and i[4].to_i == a and i[5] == g
							    if @clicks.length == 0
									@clicks = [[ad.id.to_s, 1]]
							    else
							        exist = false
									@clicks.each do |fav_ad_c|
										if fav_ad_c[0] == ad.id.to_s
										   fav_ad_c[1] = fav_ad_c[1] + 1
										   exist = true
										   break
										end
									end
									if !exist
										@clicks.push [ad.id.to_s, 1]
									end
								end
							end
						end
					end
				end
				
			    if @impressions.length > 0
					@impressions = @impressions.sort{|a,b| a[1] <=> b[1]}.reverse!
			    end
			    if @clicks.length > 0
					@clicks = @clicks.sort{|a,b| a[1] <=> b[1]}.reverse!
			    end
			    
			    @c_and_i = []
			    if @clicks.length > 0 and @impressions.length > 0
			        if @clicks.length > @impressions.length
						@clicks.each do |c|
							@c_and_i.push [c[0], 0, 0] # ID, Clicks, Impressions
						end
					else
						@impressions.each do |c|
							@c_and_i.push [c[0], 0, 0]
						end						
					end	
				elsif @impressions.length > 0
						@impressions.each do |c|
							@c_and_i.push [c[0], 0, 0]
						end						
			    end
			    
			    @c_and_i.each do |ci|
					@impressions.each do |i|
						if i[0] == ci[0]
							ci[2] = i[1]
							break
						end
					end							    
					@clicks.each do |c|
						if c[0] == ci[0]
							ci[1] = c[1]
							break
						end
					end			
			    end

			    @st.fav_ads = @c_and_i.sort{|a,b| a[1] <=> b[1]}.sort{|a,b| a[2] <=> b[2]}.reverse! # Sort By Clicks, Then By Impressions
			    if @st.fav_ads.length == 0
				   @st.fav_ads = nil
			    end
				@tables_for_save.push @st
				
    end

