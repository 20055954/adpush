namespace :events do
  desc "Monthly bills creation"
  task :create_bills => :environment do
	User.all.each do |u|
		if u.utype == 0
			if Bill.where("created_at >= ? and created_at < ?", Time.mktime(Date.today.year, Date.today.month), Time.mktime(Date.today.year, Date.today.month + 1)).where(user_id: u.id).length == 0
				Bill.new(user_id: u.id, amount: 0, paid: false).save
			end
		end
	end
  end
end
