# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( silver.css )
Rails.application.config.assets.precompile += %w( orange.css )
Rails.application.config.assets.precompile += %w( common.css )
Rails.application.config.assets.precompile += %w( jquery-ui.min.css )
Rails.application.config.assets.precompile += %w( jquery.multiselect.css )
Rails.application.config.assets.precompile += %w( logo.png )
Rails.application.config.assets.precompile += %w( inline/cookies_warning.js )
Rails.application.config.assets.precompile += %w( inline/performance.js )
Rails.application.config.assets.precompile += %w( inline/bill_pay_listener.js )
Rails.application.config.assets.precompile += %w( inline/show_served_countries.js )
Rails.application.config.assets.precompile += %w( inline/top_up_listener.js )
Rails.application.config.assets.precompile += %w( inline/withdrawal_sum_listener.js )
Rails.application.config.assets.precompile += %w( inline/products_block_single_ad.js )
Rails.application.config.assets.precompile += %w( inline/single_ad_block_listener.js )
Rails.application.config.assets.precompile += %w( inline/block_tags_helper.js )
Rails.application.config.assets.precompile += %w( inline/products_delete_block_helper.js )
Rails.application.config.assets.precompile += %w( inline/checkbox_state_setter.js )
Rails.application.config.assets.precompile += %w( inline/published_ads_hide_listener.js )
Rails.application.config.assets.precompile += %w( inline/blank_search_listener.js )
Rails.application.config.assets.precompile += %w( inline/image_show_listener.js )
Rails.application.config.assets.precompile += %w( inline/font_size_change_listener.js )
Rails.application.config.assets.precompile += %w( inline/search_results_hide_listener.js )
Rails.application.config.assets.precompile += %w( inline/color_scheme_listener.js )
Rails.application.config.assets.precompile += %w( inline/country_change_listener.js )
Rails.application.config.assets.precompile += %w( inline/resize_ad_image.js )
Rails.application.config.assets.precompile += %w( inline/multiselect_countries_ad_upload.js)
Rails.application.config.assets.precompile += %w( jquery.multiselect.min.js )
Rails.application.config.assets.precompile += %w( jquery-ui.min.js ) 
Rails.application.config.assets.precompile += %w( jquery.bpopup.min.js )
Rails.application.config.assets.precompile += %w( chartkick.js )
Rails.application.config.assets.precompile += %w( js.cookie.js ) 

