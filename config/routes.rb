Rails.application.routes.draw do
  resources :ips
  resources :ads
	resources :users
	resources :tags
	resources :products
	resources :bank_infos
	resources :invoices
	resources :feedbacks
	resources :payment_cards
	resources :suggestion_tables
	
	root 'static_pages#home'
	
    get 'signup'  => 'users#new'	
    get 'details' => 'users#edit_personal_details'
    get 'preferences' => 'users#edit_preferences'
    get 'verify_email' => 'users#verify_email'
    get 'terms_and_conditions' => 'static_pages#terms_and_conditions'
    get 'help' => 'static_pages#help'
    get 'api' => 'static_pages#api'
    get 'verification_success' => 'static_pages#verification_success'
    get 'resend_verification_email' => 'users#resend_verification_email'
    get 'login' => 'sessions#new'
    post   'login'   => 'sessions#create'
	delete 'logout'  => 'sessions#destroy'
	
	get 'inc_font' => 'users#inc_font'
	get 'dec_font' => 'users#dec_font'
	
	get 'list_tags' => 'tags#list_tags'
	get 'block_tags' => 'tags#block_tags'
	get 'block' => 'tags#block_tags_action'
    
    get 'upload' => 'ads#new'
    get 'block_ad_in_product' => 'ads#single_ad_block'
    delete 'delete_ad' => 'ads#destroy'
    get 'search' => 'ads#search'
    get 'edit' => 'ads#edit'
    
    get 'add_product' => 'products#new'
    get 'products' => 'products#index'
    get 'delete_products' => 'products#destroy'
    
    get 'add_bank_details' => 'bank_infos#new'
    get 'withdraw_revenue' => 'users#withdraw_revenue'
    post 'withdraw' => 'invoices#withdraw_revenue'
    
    get 'single_ad' => 'ads#get_ad'
    get 'random_ad' => 'ads#get_random_ad'
    get 'random_ad_tags' => 'ads#get_random_ad_by_tags'
    get 'suggested_ad' => 'ads#suggested_ad'
    get 'click_ad' => 'ads#click_ad'
    
    get 'leave_feedback' => 'feedbacks#new'
    
    get 'successful_withdrawal' => 'invoices#successful_withdrawal'
    get 'generate_invoice_pdf' => 'invoices#generate_invoice_pdf'
    
    get 'top_up' => 'users#top_up'
    post 'top_up_action' => 'users#top_up_action'
    
    get 'add_payment_card' => 'payment_cards#new'
    
    get 'pay_bills' => 'users#pay_bills'
    get 'bill_pay_invoice' => 'users#bill_pay_invoice'
    get 'generate_bill_pay_invoice_pdf' => 'users#generate_bill_pay_invoice_pdf'
    post 'pay_bills_action' => 'users#pay_bills_action'
    
    get 'performance_analysis' => 'users#performance_analysis'
end
